#!/usr/bin/env python3

import sys
from Gtools import cgtools
from numarray import *

LITTLEH = 0.7

Omega0 = 0.3
OmegaLambda = 0.7
Hubble = 0.1


tmp = array([0., 0.1, 0.2, 0.5, 1.])

print(cgtools.Age_a(tmp, Omega0, OmegaLambda, Hubble) * 0.978 / LITTLEH)
