#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      setup.py
#  brief:     Install Gtools
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################
from numpy import *

from setuptools import setup, find_packages, Extension
from glob import glob
import numpy as np
from sys import argv

# default flags
flags = ["-Werror", "-Wno-unused-parameter"]

# directory containing headers
include_dirs = [
    ".",
    np.get_include()
]

# libraries
libraries = ["m"]

# C modules
modules_name = [
    "cgtools",
]

# DO NOT TOUCH BELOW

# deal with arguments
dev = False

dev_arg = "--dev"
if dev_arg in argv:
    dev = True
    argv.remove(dev_arg)

# Generate extensions
ext_modules = []

for k in modules_name:
    # compile extension
    tmp = Extension("Gtools." + k,
                    glob("src/" + k + "/*.c"),
                    include_dirs=include_dirs,
                    libraries=libraries,
                    extra_compile_args=flags)

    ext_modules.append(tmp)


# scripts to install
scripts = glob("Gtools/pscripts/*")
scripts.extend(glob("Gtools/gscripts/*"))
scripts.extend(glob("Gtools/cscripts/*"))

exclude = []
if not dev:
    exclude.append("Gtools.dev.*")
    exclude.append("Gtools.dev")


setup(
    name="Gtools",
    author="Yves Revaz",
    author_email="yves.revaz@epfl.ch",
    url="http://obswww.unige.ch/~revaz",
    description="""Gtools module""",
    license="GPLv3",
    version="5.0",

    packages=find_packages(exclude=exclude),
    scripts=scripts,
    ext_modules=ext_modules,
    install_requires=["numpy", "scipy", "h5py",
                      "astropy", "pNbody", "matplotlib",
                      "Ptools"],

)
