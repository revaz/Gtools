#!/usr/bin/env python
'''
Extract and plot info contained in info.txt files

Yves Revaz
jeu avr  6 14:31:52 CEST 2006

'''

import os
import sys
import string
from optparse import OptionParser


import SM

from Gtools import *
from Gtools import io

from optparse import OptionParser


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = add_postscript_options(parser)
    parser = add_color_options(parser)
    parser = add_limits_options(parser)
    parser = add_log_options(parser)

    parser.add_option("--mode",
                      action="store",
                      dest="mode",
                      type="string",
                      default='normal',
                      help="mode : normal=Systemstep, NumActive",
                      metavar=" NAME")

    (options, args) = parser.parse_args()

    if options.colors is not None:
        exec("options.colors = array([%s])" % (options.colors))

    if len(args) == 0:
        print "you must specify a filename"
        sys.exit(0)

    files = args

    return files, options


#############################
# graph
#############################


# get options
files, options = parse_options()
ps = options.ps
col = options.colors
xmin = options.xmin
xmax = options.xmax
ymin = options.ymin
ymax = options.ymax
log = options.log

mode = options.mode

#######################################
# open sm
#######################################

g = Graph_Init(ps)
Graph_SetDefaultsGraphSettings(g)
colors = Graph_SetColorsForFiles(files, col)


# read files
for file in files:

    # Step,Time,Systemstep,Numactive=read_info(file)
    Step, Time, Systemstep = io.read_info(file)

    x = Time

    if mode == 'normal':
        y = Systemstep
    else:
        y = Numactive

    # use log
    if log is not None:
        x, y = Graph_UseLog(x, y, log)

    if file == files[0]:
        xmin, xmax, ymin, ymax = Graph_SetLimits(
            g, xmin, xmax, ymin, ymax, x, y)
        g.box()

    # plot points
    g.ctype(colors[file])
    g.connect(x, y)

g.ctype(0)

if mode == 'normal':
    g.xlabel('Time')
    g.ylabel('Systemstep')
else:
    g.xlabel('Time')
    g.ylabel('NumActive')

# -- end ---
Graph_End(g, ps)
