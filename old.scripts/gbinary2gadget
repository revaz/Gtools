#!/usr/bin/env python
'''
Convert binary(pfen) file into gadget file

Yves Revaz
mar oct 17 17:35:35 CEST 2006

'''

from Nbody import *
import string
from optparse import OptionParser
import sys


def toarray(s):
    n = 13
    a = []
    for i in range(6):
        a.append(float(s[i * n:(i + 1) * n]))
    return a


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-o",
                      action="store",
                      dest="outputfile",
                      type="string",
                      default=None,
                      help="output file",
                      metavar=" FILE")

    parser.add_option("--ng",
                      action="store",
                      dest="ng",
                      type="int",
                      default=0,
                      help="number of gaseous particles",
                      metavar=" INT")

    parser.add_option("--ns",
                      action="store",
                      dest="ns",
                      type="int",
                      default=0,
                      help="number of star particles",
                      metavar=" INT")

    parser.add_option("--nd",
                      action="store",
                      dest="nd",
                      type="int",
                      default=0,
                      help="number of dark particles",
                      metavar=" INT")

    parser.add_option("--mtot",
                      action="store",
                      dest="mtot",
                      type="float",
                      default=1.0,
                      help="total mass in code unit",
                      metavar=" FLOAT")

    parser.add_option("--u0",
                      action="store",
                      dest="u0",
                      type="float",
                      default=0.0,
                      help="internal energy of gaz particles",
                      metavar=" FLOAT")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print "you must specify a filename"
        sys.exit(0)

    files = args

    return files, options


#############################
# main
#############################


# get options
files, options = parse_options()
ng = options.ng
ns = options.ns
nd = options.nd
mtot = options.mtot
u0 = options.u0
outputfile = options.outputfile

try:
    nb = Nbody(files[0], ftype='binary')
except "ReadError":
    nb = Nbody(files[0], ftype='binary_dis')

nb = nb.set_ftype('gadget')
n = nb.nbody


if (ng == 0) and (nd == 0) and (ns == 0):
    nd = n

if (ng + nd + ns != n):
    print "promblem !"
    print "tot number of particles = %d" % (n)
    print "ng+nd+ns                = %d" % (ng + nd + ns)
    print "ng                      = %d" % (ng)
    print "nd                      = %d" % (nd)
    print "ns                      = %d" % (ns)
    sys.exit()

fg = (ng != 0)
fd = (nd != 0)
fs = (ns != 0)

nb.npart = [ng, nd, 0, 0, ns, 0]
nb.massarr = [mtot / n * fg, mtot / n * fd, 0, 0, mtot / n * fs, 0]
nb.nzero = 0


if (ng != 0):
    # set specific energy gas particles
    u0 = 1.
    nb.u = u0 * ones(n)


nb.rename(outputfile)
nb.write()
