#!/usr/bin/env python2

import sys
import os
import string
from optparse import OptionParser
from functools import reduce


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("--np",
                      action="store",
                      dest="np",
                      default=1,
                      type="int",
                      help="number of proc")

    parser.add_option("--nj",
                      action="store",
                      dest="nj",
                      default=2,
                      type="int",
                      help="number of job per proc")

    parser.add_option("--param",
                      action="store",
                      dest="param",
                      default='params',
                      type="string",
                      help="parameter file")

    parser.add_option("--bin",
                      action="store",
                      dest="bin",
                      default='src/Gadget2',
                      type="string",
                      help="binary file")

    parser.add_option("--nodes",
                      action="store",
                      dest="nodes",
                      default='/net/l05/home/revaz/.brshtab',
                      type="string",
                      help="nodes file")

    parser.add_option("--out",
                      action="store",
                      dest="out",
                      default='out.dat',
                      type="string",
                      help="output file")

    parser.add_option("--restart",
                      action="store_true",
                      dest="restart",
                      default=False,
                      help="restart from snapshot")

    (options, args) = parser.parse_args()

    return options

#############################
# main
#############################


# get options
options = parse_options()
np = options.np
nj = options.nj
bin = options.bin
param = options.param
nodes = options.nodes
out = options.out
restart = options.restart

# read host name
HOSTNAME = os.getenv('HOSTNAME')
# read local directory
DIRECTORY = os.path.abspath('.')


if string.find(HOSTNAME, 'luxor') != -1:

    # fullpath
    machine = HOSTNAME[0:3]
    path = os.path.join('/net', machine, DIRECTORY[1:])

    # correct bin
    bin = os.path.join(path, bin)

    # correct param
    param = os.path.join(path, param)

    # correct nodes
    if os.path.exists(os.path.join(path, nodes)):

        f = open(nodes)
        nds = f.readlines()
        nds = map(string.strip, nds)
        f.close()
        nodes = []
        n = 0
        for node in nds:
            if n == 0:
                if node == machine:

                    for i in range(nj):
                        if n < np:
                            nodes.append(node)
                            n = n + 1

            else:
                if node[0] != '#':
                    for i in range(nj):
                        if n < np:
                            nodes.append(node)
                            n = n + 1

            if n == np:
                break

        if len(nodes) < np:
            print "not enough proc ! reduce np."
            sys.exit()
        np = len(nodes)
        nodes = reduce(lambda x, y: x + " %s" % y, nodes)

    else:
        nodes = string.split(nodes, ',')
        np = len(nodes)
        nodes = reduce(lambda x, y: x + " %s" % y, nodes)

    # correct param file

    initcondfile = os.path.join(path, 'snap.dat')
    outputdir = os.path.join(path, 'snap')

    f = open(param)
    lines = f.readlines()
    f.close()

    f = open(param, 'w')
    for line in lines:

        if string.find(line, 'InitCondFile') != -1:
            line = "InitCondFile      %s\n" % (initcondfile)

        elif string.find(line, 'OutputDir') != -1:
            line = "OutputDir         %s\n" % (outputdir)

        else:
            pass

        f.write(line)

    f.close()

    # restart flag

    if restart:
        restart = 2
    else:
        restart = 0

    cmd = "mpirun_rsh -np %d %s %s %s %d &> %s & " % (
        np, nodes, bin, param, restart, out)

print
print "Num of process       = %d" % (np)
print "Num of job per proc  = %d" % (nj)
print "InitCondFile         = %s" % (initcondfile)
print "OutputDir            = %s" % (outputdir)
print "param file           = %s" % (param)
print "binary file          = %s" % (bin)
print "nodes                = %s" % (nodes)
print "cmd                  = %s" % (cmd)


print
txt = "Start the simulation ? [Y/n] "
bool = raw_input(txt)
if len(bool) == 0 or bool[0] == 'y' or bool[0] == 'Y':
    print "Starting the simulation..."
    os.system(cmd)
else:
    print "Nothing as been done. "
    sys.exit()


'''
#mpirun_rsh -np 1 -hostfile i_nod.mpi src/Gadget2 params &> out.dat &



# mpirun -np 1  src/Gadget2 params &> out.dat &
#mpirun -np 4 -machinefile l_nod.mpi /net/l05/home/revaz/glob/gl03_10/src/Gadget2 params &> out.dat &
#
#lamclean
#lamboot
#mpirun -np 4 src/Gadget2 params > out.dat&
'''
