#!/usr/bin/env python
'''
Extract and plot info contained in cpu.txt files

Yves Revaz
jeu avr  6 15:58:19 CEST 2006

'''
import Ptools as pt

import os
import sys
import string
from optparse import OptionParser


from Gtools import *
from Gtools import io
from optparse import OptionParser


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_postscript_options(parser)
    parser = pt.add_limits_options(parser)
    parser = pt.add_log_options(parser)
    parser = pt.add_cmd_options(parser)

    parser.add_option("--mode",
                      action="store",
                      dest="mode",
                      type="string",
                      default='total',
                      help="mode : cpu, cpu/step",
                      metavar=" NAME")

    parser.add_option("--legend",
                      action="store_true",
                      dest="legend",
                      default=False,
                      help="add a legend")

    parser.add_option("-t",
                      action="store",
                      dest="time",
                      type="string",
                      default='hour',
                      help="time : day, hour, minute, second",
                      metavar=" TYPE")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print "you must specify a filename"
        sys.exit(0)

    files = args

    return files, options


#######################################
# MakePlot
#######################################


def MakePlot(dirs, opt):

    # some inits
    colors = pt.Colors()
    linestyles = pt.LineStyles(n=len(files))
    datas = []

    # read files
    for file in files:

        linestyle = linestyles.get()

        try:
            vals = pt.io.read_ascii(file)
        except BaseException:
            Step, Time, CPUs, CPU_Total, CPU_Gravity, CPU_Hydro, CPU_Domain, CPU_Potential, CPU_Predict, CPU_TimeLine, CPU_Snapshot, CPU_TreeWalk, CPU_TreeConstruction, CPU_CommSum, CPU_Imbalance, CPU_HydCompWalk, CPU_HydCommSumm, CPU_HydImbalance, CPU_EnsureNgb, CPU_PM, CPU_Peano = io.read_cpu(
                file)

            vals = {}
            vals["Step"] = Step
            vals["Time"] = Time
            vals["nCPUs"] = CPUs
            vals["CPU_Total"] = CPU_Total
            vals["CPU_Gravity"] = CPU_Gravity
            vals["CPU_Hydro"] = CPU_Hydro
            vals["CPU_Domain"] = CPU_Domain
            vals["CPU_Potential"] = CPU_Potential
            vals["CPU_Predict"] = CPU_Predict
            vals["CPU_TimeLine"] = CPU_TimeLine
            vals["CPU_Snapshot"] = CPU_Snapshot
            vals["CPU_TreeWalk"] = CPU_TreeWalk
            vals["CPU_TreeConstruction"] = CPU_TreeConstruction
            vals["CPU_CommSum"] = CPU_CommSum
            vals["CPU_Imbalance"] = CPU_Imbalance
            vals["CPU_HydCompWalk"] = CPU_HydCompWalk
            vals["CPU_HydCommSumm"] = CPU_HydCommSumm
            vals["CPU_HydImbalance"] = CPU_HydImbalance
            vals["CPU_EnsureNgb"] = CPU_EnsureNgb
            vals["CPU_PM"] = CPU_PM
            vals["CPU_Peano"] = CPU_Peano

        Time = vals["Time"]
        nCPUs = vals["nCPUs"]

        ##############
        # global bilan
        ##############

        if opt.mode == 'total':

            Gravity = vals["CPU_Gravity"] / vals["CPU_Total"]
            Hydro = vals["CPU_Hydro"] / vals["CPU_Total"]
            Domain = vals["CPU_Domain"] / vals["CPU_Total"]
            Potential = vals["CPU_Potential"] / vals["CPU_Total"]
            Predict = vals["CPU_Predict"] / vals["CPU_Total"]
            TimeLine = vals["CPU_TimeLine"] / vals["CPU_Total"]
            Snapshot = vals["CPU_Snapshot"] / vals["CPU_Total"]
            Peano = vals["CPU_Peano"] / vals["CPU_Total"]

            datas.append(
                pt.DataPoints(
                    Time,
                    Gravity,
                    color='r',
                    linestyle=linestyle,
                    label='Gravity'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Hydro,
                    color='g',
                    linestyle=linestyle,
                    label='Hydro'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Domain,
                    color='b',
                    linestyle=linestyle,
                    label='Domain'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Potential,
                    color='c',
                    linestyle=linestyle,
                    label='Potential'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Predict,
                    color='m',
                    linestyle=linestyle,
                    label='Predict'))
            datas.append(
                pt.DataPoints(
                    Time,
                    TimeLine,
                    color='y',
                    linestyle=linestyle,
                    label='TimeLine'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Snapshot,
                    color='k',
                    linestyle=linestyle,
                    label='Snapshot'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Peano,
                    color='k',
                    linestyle=linestyle,
                    label='Peano'))

            if 'CPU_Chimie' in vals:
                Chimie = vals["CPU_Chimie"] / vals["CPU_Total"]
                datas.append(
                    pt.DataPoints(
                        Time,
                        Chimie,
                        color='y',
                        linestyle=linestyle,
                        label='Chimie'))

            if 'CPU_StarFormation' in vals:
                StarFormation = vals["CPU_StarFormation"] / vals["CPU_Total"]
                datas.append(
                    pt.DataPoints(
                        Time,
                        StarFormation,
                        color='b',
                        linestyle=linestyle,
                        label='StarFormation'))

        ##############
        # gravity bilan
        ##############

        elif opt.mode == 'gravity':

            TreeWalk = vals["CPU_TreeWalk"] / vals["CPU_Gravity"]
            TreeConstruction = vals["CPU_TreeConstruction"] / \
                vals["CPU_Gravity"]
            CommSum = vals["CPU_CommSum"] / vals["CPU_Gravity"]
            Imbalance = vals["CPU_Imbalance"] / vals["CPU_Gravity"]

            datas.append(
                pt.DataPoints(
                    Time,
                    TreeWalk,
                    color='r',
                    linestyle=linestyle,
                    label='TreeWalk'))
            datas.append(
                pt.DataPoints(
                    Time,
                    TreeConstruction,
                    color='g',
                    linestyle=linestyle,
                    label='TreeConstruction'))
            datas.append(
                pt.DataPoints(
                    Time,
                    CommSum,
                    color='b',
                    linestyle=linestyle,
                    label='CommSum'))
            datas.append(
                pt.DataPoints(
                    Time,
                    Imbalance,
                    color='k',
                    linestyle=linestyle,
                    label='Imbalance'))

        ##############
        # gas bilan
        ##############

        elif opt.mode == 'hydro':

            print "warning, CPU_HydCompWalk,CPU_HydCommSumm,CPU_HydImbalance, CPU_EnsureNgb "
            print "are means of total time"

            HydCompWalk = vals["CPU_HydCompWalk"] / vals["CPU_Hydro"]
            HydCommSumm = vals["CPU_HydCommSumm"] / vals["CPU_Hydro"]
            HydImbalance = vals["CPU_HydImbalance"] / vals["CPU_Hydro"]
            EnsureNgb = vals["CPU_EnsureNgb"] / vals["CPU_Hydro"]

            datas.append(
                pt.DataPoints(
                    Time,
                    HydCompWalk,
                    color='r',
                    linestyle=linestyle,
                    label='HydCompWalk'))
            datas.append(
                pt.DataPoints(
                    Time,
                    HydCommSumm,
                    color='g',
                    linestyle=linestyle,
                    label='HydCommSumm'))
            datas.append(
                pt.DataPoints(
                    Time,
                    HydImbalance,
                    color='b',
                    linestyle=linestyle,
                    label='HydImbalance'))
            datas.append(
                pt.DataPoints(
                    Time,
                    EnsureNgb,
                    color='k',
                    linestyle=linestyle,
                    label='EnsureNgb'))

            pt.legend(['HydCompWalk', 'HydCommSumm',
                       'HydImbalance', 'EnsureNgb'])

    # now, plot
    for d in datas:
        pt.plot(d.x, d.y, color=d.color, linestyle=d.linestyle)

    # set limits and draw axis
    xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(
        opt.xmin, opt.xmax, opt.ymin, opt.ymax, datas, opt.log)
    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)

    if opt.legend:
        pt.LegendFromDataPoints(datas)

    pt.xlabel('Time')
    pt.ylabel('Fraction')
    pt.title('Runned on %d CPUs' % (nCPUs[0]))

    del vals


if __name__ == '__main__':
    files, opt = parse_options()
    pt.InitPlot(files, opt)
    # pt.figure(figsize=(8*2,6*2))
    # pt.figure(dpi=10)

    #fig = pt.gcf()
    # fig.subplots_adjust(left=0.1)
    # fig.subplots_adjust(right=1)
    # fig.subplots_adjust(bottom=0.12)
    # fig.subplots_adjust(top=0.95)
    # fig.subplots_adjust(wspace=0.25)
    # fig.subplots_adjust(hspace=0.02)

    MakePlot(files, opt)
    pt.EndPlot(files, opt)
