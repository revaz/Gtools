#!/usr/bin/env python
'''
Plot an histogram of angular momentum of particles

Yves Revaz
Wed Aug 31 14:31:49 CEST 2005

'''


from numarray import *
from Nbody import *
import SM
import string
import sys
import os

from libjeans import *

from Nbody.libutil import histogram


try:
    from optparse import OptionParser
except ImportError:
    from optik import OptionParser


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-t",
                      action="store",
                      dest="ftype",
                      type="string",
                      default=None,
                      help="type of the file",
                      metavar=" TYPE")

    parser.add_option("--lmin",
                      action="store",
                      dest="lmin",
                      type="float",
                      default=None,
                      help="min value in Lz")

    parser.add_option("--lmax",
                      action="store",
                      dest="lmax",
                      type="float",
                      default=None,
                      help="max value in Lz")

    parser.add_option("--dl",
                      action="store",
                      dest="dl",
                      type="float",
                      default=None,
                      help="interval in Lz")

    parser.add_option("--miny",
                      action="store",
                      dest="mny",
                      type="float",
                      default=None,
                      help="min value in y")

    parser.add_option("--maxy",
                      action="store",
                      dest="mxy",
                      type="float",
                      default=None,
                      help="max value in y")

    parser.add_option("--gas",
                      action="store_true",
                      dest="gas",
                      default=0,
                      help="keep only gas")

    parser.add_option("-p",
                      action="store",
                      dest="ps",
                      type="string",
                      default=None,
                      help="postscript filename",
                      metavar=" FILE")

    parser.add_option("-c",
                      action="store",
                      dest="colors",
                      type="string",
                      default=None,
                      help="colors",
                      metavar=" 0,64,192")

    (options, args) = parser.parse_args()

    if options.colors is not None:
        exec("options.colors = array([%s])" % (options.colors))

    if len(args) == 0:
        print "you must specify a filename"
        sys.exit(0)

    files = args

    return files, options


#############################
# graph
#############################


# get options
files, options = parse_options()
ps = options.ps
lmin = options.lmin
lmax = options.lmax
dl = options.dl
hmin = options.mny
hmax = options.mxy
col = options.colors
ftype = options.ftype
gas = options.gas

# set colors
colors = {}
i = 0
for file in files:
    if col is not None:
        colors[file] = col[i]
    else:
        colors[file] = i * 255 / len(files)

    i = i + 1


#######################################
# open sm
#######################################
if ps is None:
    g = SM.plot("x11 -bg white -fg black ")
else:
    g = SM.plot("postencap %s" % ps)

# some init
g.palette('bgyrw')
g.expand(0.999)
g.setvariable('TeX_strings', '1')


#######################################
# first

g.location(3500, 31000, 3500, 31000)


#######################################
# LOOP
#######################################

vs = array([], Float)
ts = array([], Float)

for file in files:

    # open file
    nb = Nbody(file, ftype=ftype)
    print nb.tnow
    nb.histocenter()
    if gas:
        nb = nb.select('gas')

    lz = nb.l()[:, 2]

    if lmin is None:
        lmin = min(lz)

    if lmax is None:
        lmax = max(lz)

    if dl is None:
        dl = (lmax - lmin) / 50.

    bins = arange(lmin, lmax, dl)
    h = histogram(lz, bins)

    if hmin is None:
        hmin = min(h)

    if hmax is None:
        hmax = max(h)

    if file == files[0]:
        g.location(3500, 31000, 3500, 31000)
        g.limits(lmin, lmax, hmin, hmax)
        g.box()

    # color
    g.ctype(colors[file])

    g.histogram(bins, h)


g.ctype(0)
g.xlabel('l')
g.ylabel('N')

# -- end ---
if ps is None:
    g.show()
else:
    g.write()
    g.clean()
