#!/home/revaz/local/bin/python
'''
Plot temperature of the model as a function of density
assuming an ideal gas


Yves Revaz
mer sep 13 14:18:04 CEST 2006

'''

from numarray import *
from Nbody import *
import SM
import string
import sys
import os

from libjeans import *

from Nbody.libutil import histogram

from optparse import OptionParser
from Gtools import *
from Gtools import vanderwaals as vw


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = add_postscript_options(parser)
    parser = add_color_options(parser)
    parser = add_limits_options(parser)
    parser = add_units_options(parser)
    parser = add_log_options(parser)
    parser = add_histogram_options(parser)
    parser = add_reduc_options(parser)
    parser = add_gas_options(parser)
    parser = add_center_options(parser)
    parser = add_select_options(parser)

    parser.add_option("-t",
                      action="store",
                      dest="ftype",
                      type="string",
                      default=None,
                      help="type of the file",
                      metavar=" TYPE")

    parser.add_option("--cgs",
                      action="store_true",
                      dest="cgs",
                      default=0,
                      help="invert into cgs units")

    (options, args) = parser.parse_args()

    if options.colors is not None:
        exec("options.colors = array([%s])" % (options.colors))

    if len(args) == 0:
        print "you must specify a filename"
        sys.exit(0)

    files = args

    return files, options


#############################
# graph
#############################


# get options
files, options = parse_options()
ps = options.ps
col = options.colors
xmin = options.xmin
xmax = options.xmax
ymin = options.ymin
ymax = options.ymax
log = options.log
histogram = options.histogram
reduc = options.reduc

cgs = options.cgs
ftype = options.ftype

gamma = options.gamma
mu = options.mu
av = options.av
bv = options.bv

center = options.center
select = options.select

localsystem = Set_SystemUnits_From_Options(options)

#######################################
# define output system of unit
#######################################

outputunits = UnitSystem(
    'mks', [Unit_kpc, Unit_Ms, Unit_yr, Unit_K, Unit_mol, Unit_C])


#######################################
# open sm
#######################################

g = Graph_Init(ps)
Graph_SetDefaultsGraphSettings(g)
colors = Graph_SetColorsForFiles(files, col)


#######################################
# LOOP
#######################################

# read files
for file in files:

    nb = Nbody(file, ftype=ftype)

    # center the model
    if center == 'hdcenter':
        print "centering %s" % (center)
        nb.hdcenter()
    elif center == 'histocenter':
        print "centering %s" % (center)
        nb.histocenter()

    # select
    if select is not None:
        print "select %s" % (select)
        nb = nb.select(select)

    if reduc is not None:
        print "reducing %s" % (reduc)
        nb = nb.reduc(reduc)

    # get values
    nb.localsystem_of_units = localsystem
    x = nb.rho.astype(Float)
    y = nb.S()

    # use log
    if log is not None:
        x, y = Graph_UseLog(x, y, log)
        g.ticksize(-1, 0, -1, -1)

    if file == files[0]:
        xmin, xmax, ymin, ymax = Graph_SetLimits(
            g, xmin, xmax, ymin, ymax, x, y)
        Graph_DrawBox(g, xmin, xmax, ymin, ymax, log)

    g.ctype(colors[file])

    # plot points
    if histogram:
        xb, yb = Graph_MakeHistrogram(g, xmin, xmax, ymin, ymax, x, y)
        g.connect(xb, yb)
    else:
        g.points(x, y)


g.ctype(0)
if log == 'xy' or log == 'yx':
    g.xlabel('log Density')
    g.ylabel('log S')
elif log == 'x':
    g.xlabel('log Density')
    g.ylabel('S')
elif log == 'y':
    g.xlabel('Density')
    g.ylabel('log S')
else:
    g.xlabel('Density')
    g.ylabel('S')


# -- end ---
Graph_End(g, ps)
