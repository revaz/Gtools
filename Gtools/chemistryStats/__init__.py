#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      chemistryStats/__init__.py
#  brief:     chemistryStats init file
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# 
#  This file is part of Gtools.
###########################################################################################

import glob
import sys
import os
import pickle
import numpy as np


def IsFileEmpty(f):
  """
  return True if the file has only one line, False instead
  
  f: file name
  """
  f = open(f,"r")
  size1 = f.seek(0, 2)
  f.seek(0)
  f.readline()
  size2 = f.tell()
  f.close()
  
  if size1==size2:
    return True
  else:
    return False
    
  


def ReadChimieStatsGas(data_dir,opt):
  '''
  read chimie.txt_Gas.* files
  
  keep all entries for a gas particle
  
  '''
  
  
  data_gas = {}
  
  files = glob.glob(os.path.join(data_dir,"chimie.txt_Gas.*"))
  
  # header
  f = open(files[0],'r')
  l = f.readline()
  h = l.split()[1:]
  f.close()
  
  idx = h.index("Mass_0") 
  
  element_list = h[idx:]
  
  if opt.elts is not None:
    opt.elts.insert(0,"Mass")
  
    elts = []
    for elt in opt.elts:
      elts.append("%s_0"%elt)
    for elt in opt.elts:
      elts.append("%s_1"%elt)    
    
    element_list = elts
  

  
  for f in files:
    
    if IsFileEmpty(f):
      continue
      
    data = np.loadtxt(f,comments='#')

    if data.size==0:
      print("Skipping %s"%f)
      continue
    
    while len(data) > 0:
    
      # take the first ID of the list
      gas_IDs = data[:,h.index("ID")].astype(int)
      ID = gas_IDs[0]
      #print(ID)
    
      # get a subset of
      c = gas_IDs == ID
      sub_data = np.compress(c,data,axis=0)
      
      # remove the gas particle from the total data
      data = np.compress(np.invert(c),data,axis=0)
    
      if ID in data_gas.keys():                                     # we have recorded the gas already
        data_gas[ID] = np.concatenate((data_gas[ID],sub_data))      
      else:
        data_gas[ID] = sub_data       

      #print("len = ",len(data))   
  
  
  # sort according to the time
  for key in data_gas.keys():
    s = np.argsort(data_gas[key][:,h.index("Time")])
    data_gas[key] = data_gas[key][s]
  

  # create a dictionary of dictionary
  for key in data_gas.keys():
    
    data = data_gas[key]
    
    data_gas[key] = {}
    
    data_gas[key]["TimeStep"]       = (data[:,h.index("TimeStep")]).astype(int)
    data_gas[key]["Time"]           =  data[:,h.index("Time")]
    data_gas[key]["IDStar"]         = (data[:,h.index("IDStar")]).astype(int)
    data_gas[key]["StarMeanMass"]   =  data[:,h.index("StarMeanMass")]
    data_gas[key]["StarFeH"]        =  data[:,h.index("StarFeH")]
    data_gas[key]["aij"]            =  data[:,h.index("aij")]
    
    data_gas[key]["x"]              =  data[:,h.index("x")]
    data_gas[key]["y"]              =  data[:,h.index("y")]
    data_gas[key]["z"]              =  data[:,h.index("z")]

    #data_gas[key]["Mass_0"]         =  data[:,h.index("Mass_0")]
    #data_gas[key]["Fe_0"]           =  data[:,h.index("Fe_0")]
    #data_gas[key]["Mg_0"]           =  data[:,h.index("Mg_0")]

    #data_gas[key]["Mass_1"]         =  data[:,h.index("Mass_1")]
    #data_gas[key]["Fe_1"]           =  data[:,h.index("Fe_1")]
    #data_gas[key]["Mg_1"]           =  data[:,h.index("Mg_1")]
            
    # loop over all elements find in element_list
    for elt in element_list:
      data_gas[key][elt]         =  data[:,h.index(elt)]
      



  return data_gas


def ReadChimieStatsStar(data_dir,opt):
  '''
  read chimie.txt_Star.* files
  '''
  
  data_star = {}
  
  files = glob.glob(os.path.join(data_dir,"chimie.txt_Star.*"))

  # header
  f = open(files[0],'r')
  l = f.readline()
  h = l.split()[1:]
  f.close()  
  
  for f in files:

    if IsFileEmpty(f):
      continue
    
    data = np.loadtxt(f,comments='#')  

    if data.size==0:
      print("Skipping %s"%f)
      continue
    
    if data.ndim == 1:
      data = np.array([data])
        
    for i in range(len(data)):
      
      ID = int(data[i][h.index("ID")])
      
      # count the number of particles that participated to the smooth
      # 8 is the number of data before the rest
      # 4 is the number of the fields for those particles
      nf1 = 8
      if opt.old_format:
        nf2 = 3
      else:
        nf2 = 4
      
      n = (len(data[i]) - nf1)//nf2      
      d = data[i][nf1:]
      
      IDs = np.zeros(n).astype(int)
      aij = np.zeros(n)
      Zaij = np.zeros(n)
      mass = np.zeros(n)
      
      for j in range(n):
        IDs[j]  = data[i][nf1+nf2*j]
        aij[j]  = data[i][nf1+nf2*j+1]
        Zaij[j] = data[i][nf1+nf2*j+2]
        if not opt.old_format:
          mass[j] = data[i][nf1+nf2*j+3]
      
      
      data_star[ID] = {}
      data_star[ID]["TimeStep"]   = int(data[i][1])
      data_star[ID]["Time"]       = data[i][2]
      data_star[ID]["IDprog"]     = int(data[i][4])
      data_star[ID]["flagToStar"] = int(data[i][5])
      data_star[ID]["IDs"]        = IDs.astype(int)
      data_star[ID]["aij"]        = aij
      data_star[ID]["Zaij"]       = Zaij
      if not opt.old_format:
        data_star[ID]["mass"]     = mass
      
    
  return data_star      

def ReadChimieStatsSNs(data_dir,opt):
  '''
  read chimie.txt_SNs.* files
  
  keep all entries for a gas particle
  
  '''
  
  
  data_SNs = {}
  
  files = glob.glob(os.path.join(data_dir,"chimie.txt_SNs.*"))
  
  # header
  f = open(files[0],'r')
  l = f.readline()
  h = l.split()[1:]
  f.close()
  
  # extract list of ejected elements (e.g. "EjectedMass_Fe")
  ejected_elts = []
  for elt in h:
    if elt.find("_") > -1:
      ejected_elts.append(elt)
  

  
  for f in files:

    if IsFileEmpty(f):
      continue
          
    data = np.loadtxt(f,comments='#')
    
    if data.size==0:
      print("Skipping %s"%f)
      continue  
    
    while len(data) > 0:
    
      if data.ndim==1:
        data.shape = (1,data.size)
        
      # take the first ID of the list
      SNs_IDs = data[:,h.index("ID")].astype(int)
      ID = SNs_IDs[0]
    
      # get a subset of
      c = SNs_IDs == ID
      sub_data = np.compress(c,data,axis=0)
      
      
      # remove the gas particle from the total data
      data = np.compress(np.invert(c),data,axis=0)
    
      if ID in data_SNs.keys():     # we have recorded the gas already
        data_SNs[ID] = np.concatenate((data_SNs[ID],sub_data))              # here, we should sort according to the time
      else:
        data_SNs[ID] = sub_data       

      #print("len = ",len(data))   
      
  
  # create a dictionary of dictionary
  for key in data_SNs.keys():
    
    data = data_SNs[key]
    
    data_SNs[key] = {}
    
    data_SNs[key]["TimeStep"]       = (data[:,h.index("TimeStep")]).astype(int)
    data_SNs[key]["Time"]           =  data[:,h.index("Time")]
    data_SNs[key]["Mass"]           =  data[:,h.index("Mass")]
    data_SNs[key]["NSNII"]           =  data[:,h.index("NSNII")].astype(int)
    data_SNs[key]["NSNIa"]           =  data[:,h.index("NSNIa")].astype(int)
    data_SNs[key]["MassSN"]           =  data[:,h.index("MassSN")]
    data_SNs[key]["x"]           =  data[:,h.index("x")]
    data_SNs[key]["y"]           =  data[:,h.index("y")]
    data_SNs[key]["z"]           =  data[:,h.index("z")]
    data_SNs[key]["EjectedGasMass"]           =  data[:,h.index("EjectedGasMass")]
    
    # loop over ejected elements
    for ejected_elt in ejected_elts:
      data_SNs[key][ejected_elt]           =  data[:,h.index(ejected_elt)]
    
    #data_SNs[key]["EjectedMass_Fe"]       =  data[:,h.index("EjectedMass_Fe")]
    #data_SNs[key]["EjectedMass_Mg"]       =  data[:,h.index("EjectedMass_Mg")]
    #data_SNs[key]["EjectedMass_O"]        =  data[:,h.index("EjectedMass_O")]
    #data_SNs[key]["EjectedMass_C"]        =  data[:,h.index("EjectedMass_C")]
    #data_SNs[key]["EjectedMass_Al"]       =  data[:,h.index("EjectedMass_Al")]
    #data_SNs[key]["EjectedMass_Ca"]       =  data[:,h.index("EjectedMass_Ca")]
    #data_SNs[key]["EjectedMass_Ba"]       =  data[:,h.index("EjectedMass_Ba")]
    #data_SNs[key]["EjectedMass_Zn"]       =  data[:,h.index("EjectedMass_Zn")]
    #data_SNs[key]["EjectedMass_Eu"]       =  data[:,h.index("EjectedMass_Eu")]
    #data_SNs[key]["EjectedMass_Metals"]   =  data[:,h.index("EjectedMass_Metals")]

  return data_SNs

def ReadChimiePickleFile(f):
  
  f = open(f,"rb")
  data_Star = pickle.load(f)
  data_Gas  = pickle.load(f)
  data_SNs  = pickle.load(f)
  f.close()
  
  return data_Star,data_Gas,data_SNs


ChimieSolarMassAbundances={'Fe': 0.0017660372,
                          'C': 0.0039772657,
                          'Na': 4.915132e-05,
                          'Mg': 0.00092431647,
                          'Al': 7.962805e-05,
                          'Si': 0.0009965289,
                          'K': 5.15412e-06,
                          'Ca': 9.181797e-05,
                          'Sc': 6.64948e-08,
                          'Ti': 5.015746e-06,
                          'V': 5.0942e-07,
                          'Cr': 2.4320356e-05,
                          'Mn': 1.348568e-05,
                          'Co': 4.90185e-06,
                          'Ni': 0.00010440278,
                          'Cu': 1.0305954e-06,
                          'Zn': 2.6028247e-06,
                          'Metals': 0.02}





def MgFe(Fe,Mg):
  
  FeSol = ChimieSolarMassAbundances["Fe"]
  MgSol = ChimieSolarMassAbundances["Mg"]
  eps=1e-20
  
  return np.log10((Mg+eps)/(Fe+eps) /MgSol*FeSol)
  
  
def Fe(Fe):
  
  FeSol = ChimieSolarMassAbundances["Fe"]
  eps=1e-20
  
  return np.log10(Fe/FeSol+eps)


def ElementsRatio(name1,elt1,name2,elt2):
  
  elt1Sol = ChimieSolarMassAbundances[name1]
  elt2Sol = ChimieSolarMassAbundances[name2]
  eps=1e-20
  
  return np.log10((elt2+eps)/(elt1+eps) /elt2Sol*elt1Sol)








