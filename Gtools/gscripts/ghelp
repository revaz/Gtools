#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      ghelp
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################

from sys import argv

gscript = """
################################################
		gscripts/
################################################


################################################
# utilities

# cosmic time as a function of a, da or Dloga

gatime2Myr --param params --a1 0.01  --a2 0.02
gatime2Myr --param params --a 0.1914 --da 7.56271e-09
gatime2Myr --param params --a 0.1914 --Dloga 3.95127e-08

# cosmic time as a function of Z

gZtoGyr --param params -Z 2.1
gZtoMyr --param params -Z 2.1


################################################
# run preparation

# set output timesteps for cosmological simulations

gmktimesteps -p 10 > outputtimesteps.txt


# set the number of particles and masses for a cosmological simulation

gSetCosmoBox
gSetCosmoBox --StellarMass 512  --lmax 14
gSetCosmoBox --BoxSize 100
gSetCosmoBox --zoom
# agora MW-Zoom
gSetCosmoBox --StellarMass 43421.29142 --lmax 7 --lmax 12 --zoom



# setup initial temperature for cosmological simulations

ggetInitTemperatureforCosmoSims --param params --Z 70

# setup initial redshift for (zoom-in) cosmological simulations

ggetZiforCosmoSims --param params --N2 512 --N1 64 -L 3.442



# return some info on the header (incomplete, actually return the atime)

gread_header	snap.dat


# return some info on the header (for hdf5 format)

gheader snap.hdf5


################################################
# file conversion

# gadget to gear
gad2gear snap.dat gear-file.dat


# gadget to swift
gad2swift snap.dat swift-file.hdf5


################################################
# generate initial conditions

# plummer
ic_plummer -N 100000 --Mtot 1e-5 -a 0.1 --Rmax 10 -t swift -o plummer.hdf5

# nfw
ic_nfw -N 730000 --Rmin 1e-3 --Rmax 100 --rho0 0.1 -a 0.1  -t swift -o nfw.hdf5

# hernquist
ic_hernquist -N 730000 --Rmin 1e-3 --Rmax 100 --rho0 0.1 -a 0.1  -t swift -o hernquist.hdf5

# multi component model
ic_nfw+plummer --Rmin 1e-3 --Rmax 30  --rho0 0.007 --rc 0.5   --Mtot 1e5 -a 0.1 --ptype1 4 --ptype2 1  --mass1 1000 --mass2 1000 -t swift -o nfw+plummer.hdf5


"""


stat = """
################################################
# global statitics on the run

gcpu      : plot cpu info

gcpu                      cpu.txt
gcpu -o budget  --legend  cpu.txt
gcpu -o gravity --legend  cpu.txt
gcpu -o hydro   --legend  cpu.txt




genergy  	: plot info relative to the energy

genergy --relative                energy.txt
genergy -o EnergyThermalFeedback  energy.txt


gplot_energy	: plot info relative to the energy budget

gplot_energy --legend             energy.txt



gsteps		: plot info relative to the steps

gsteps                                    info.txt
gsteps --param params  --log y  --rf 100  info.txt



gSoftening : give the softening for a given scale factor

gSoftening --comoving_softening 0.0714  --max_physical_softening  0.0187 -a 0.14

"""
pscript = """
################################################
pscripts/
################################################


################################################
# global output analysis

# star formation rate and stellar mass (sfr.txt)

pSfrvsTime --param params -o SFR            sfr.txt
pSfrvsTime --param params -o MSTARS         sfr.txt
pSfrvsTime --param params -o MNEWSTARS      sfr.txt

pSfrvsTime --param params -o sfr            sfr.txt
pSfrvsTime --param params -o mnewstars --integrate --derive --rf 100  sfr.txt
pSfrvsTime --param params -o mstars         sfr.txt


# supernovae				(chimie.txt)
gSNvsTime -o NSNII,NSNIa --rf 1000                  chimie.txt
gSNvsTime -o EgySNThermal,EgySNKinetic --rf 1000    chimie.txt
gSNvsTime -o EgySNThermal,EgySNKinetic --integrate  chimie.txt
gSNvsTime -o Nflag --rf 1000                        chimie.txt
gSNvsTime -o Nflag --integrate                      chimie.txt


# accretion   (accretion.txt)
gAccretionvsTime -o MT                accretion.txt
gAccretionvsTime -o MT_th             accretion.txt
gAccretionvsTime -o DMDT              accretion.txt
gAccretionvsTime -o DMDT_th           accretion.txt


################################################
# unit conversion

# change units, including the little h (Hubble parameter) if it is different than 1
gchangeunits --param1 params --param2 params.iso  snap.dat -o snap2.dat

# correct from scaling factor (and add the correct time in code unit)
gcosmo2giso  --param1 params --param2 params.iso  snap2.dat -o snap3.dat



################################################
# snapshot analysis


# luminosity
pget_Luminosity --param params snap.dat
pget_Luminosity --param params.iso --tnow 3000 snap.iso.dat

# virial radius
pget_VirialRadius --param params --dX 200 snap.dat


# star formation rate
pSFRvsTime --param params  -o SFR  --xmin=0 --xmax=14  snap.dat

# mass vs time
pSFRvsTime --param params  -o MSTARS   --xmin=0 --xmax=14   snap.dat
pSFRvsTime --param params  -o MSTARSz  --xmin=0 --xmax=10   snap.dat
pSFRvsTime --param params  -o MSTARSa  --xmin=0 --xmax=1    snap.dat

# SFR + Mass
pSFRvsTime --param params -o BOTH --xmin=0 --xmax=14 snap.dat


# logRho - logT
pplot --param params  --select=gas --x logrho --y logT  --xmin -7 --xmax 7 --ymin 1 --ymax 7          gas.dat
pplot --param params  --select=gas --x logrho --y logT  --xmin -7 --xmax 7 --ymin 1 --ymax 7   --map  gas.dat
pplot --param params  --select=gas --colorbar --log z --x logrho --y logT --z Tcool  --xmin -3 --xmax 0 --ymin 1 --ymax 5 --zmin 1 --zmax 1e16  gas.dat

# rho - T
pplot --param params  --select=gas --x rho --y T --log xy --xmin 1e-3 --xmax 1e0 --ymin 1e1 --ymax 1e5  gas.dat

# logrho - logP
pplot --param params  --select=gas --x logrho --y logP --xmin -7 --xmax 7  gas.dat

# R - T
pplot --param params  --select=gas --x R   --y logT                       gas.dat

# R - Fe
pplot --param params  --select=gas --x R    --y Fe                        gas.dat



# MgFe - Fe
pplot --param params  --select=stars1 --x Fe   --y MgFe --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5       snap.dat
pplot --param params  --select=stars1 --x Fe   --y MgFe --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5 --map snap.dat


# Fe - Age
pplot --param params  --select=stars1 --x Age  --y Fe --xmin=0 --xmax=14 --ymin=-4 --ymax=0.5           snap.dat



# Tcool
pplot --param params  --select=gas    --x logrho  --y Tcool --xmin=-5 --xmax=2  --ymin 0 --ymax 1e16  --log y snap.dat


# gas fraction as a function of parameters (in dev), use results from pMassGasvsTime
# x->density, y->temperature
pCompareMassFraction --legend --x rho --y T out.pickle -p testCompare.png
# x->wind velocity
pCompareMassFraction --legend --x rho --y u out.pickle -p testCompare.png


# IMF
# plot IMF from stars
pIMF                                        snapshot.hdf5
# add current mass
pIMF --add_current_mass                     snapshot.hdf5
# fit IMF and provides info
pIMF --add_current_mass --IMF               snapshot.hdf5
# select stars according to their age
pIMF --add_current_mass --IMF --age_min 200 snapshot.hdf5




################################
# metallicite
################################

# MgFevsFe
pMgFevsFe            --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5 --map       snap.dat

# BaFevsFe
pBaFevsFe            --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5 --map       snap.dat


# [E1/E2] vs [E3/E4]
pplot                --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5 --map --x Fe/H --y Ba/Fe  snap.dat



# NvsAge
pNvsX --param params --select stars1  --x Age --nx 14 --xmin=0 --xmax=14                          snap.dat

# NvsFe
pNvsX --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15  snap.dat



################################
# luminosity profile
################################


pLuminosityProfile --nr 64  --HubbleParameter=0.673  --select stars1  --param params              snap.dat



################################
# profiles
################################

# grille spherique
# density
pSphericalProfile -o density --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params     snap.dat

# masse
pSphericalProfile -o mass    --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params     snap.dat

# integrated mass
pSphericalProfile -o imass   --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params     snap.dat

# velocity disp. in z
pSphericalProfile -o sigmaz  --xmax 10 --rmax 10 --nr 64 --center histocenter          --param params     snap.dat

# circular velocity
pSphericalProfile -o vcirc   --xmax 10 --rmax 10 --nr 64 --center histocenter          --param params --eps 0.1   snap.dat


# grille cylindrique
# density
pCylindricalProfile   -o sdens --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params   snap.dat

# masse
pCylindricalProfile -o mass    --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params   snap.dat

# integrated mass
pCylindricalProfile -o imass   --xmax 10 --rmax 10 --nr 64 --center histocenter --log xy --param params   snap.dat

# velocity disp. in z
pCylindricalProfile -o sigmaz  --xmax 10 --rmax 10 --nr 64 --center histocenter          --param params   snap.dat

# velocity disp. in z (irregular grid)
pCylindricalProfile -o sigmaz --xmax 0.15 --rmax 0.15 --npb1 30 --npb2 100 --ymin 0 --nlos 50 snap.dat

# circular velocity
pCylindricalProfile -o vcirc   --xmax 10 --rmax 10 --nr 64 --center histocenter          --param params --eps 0.1   snap.dat

# luminosity profile
pCylindricalProfile -o Lum     --xmax 10 --rmax 10 --nr 64 --center histocenter --select stars1  --param params     snap.dat

############################################################
# velocities and velocity dispertions in cylindrical coords
############################################################

# extract all
gExtractCylindrical --Rmax 10 --components="('gas','stars1','halo1')"   --disk="('gas','stars1')" --nR 32 --eps=0.01 --nmin 3  --param params snap.dat


# number of points per bins
pplotdmp --mode=Numbers     gas.dmp

# circular velocity per component
pplotdmp --mode=Velocities --legend   gas.dmp stars1.dmp halo1.dmp

# velocities and velocity disp
pplotdmp --mode=Dispersions --legend                    gas.dmp

# stability Toomre paramter Q
pplotdmp --mode=Q   gas.dmp


# quiver plot
pPlotQuiverVelocity --ids data.num --nber_arrow 1000 --keep 1


################################
# Standard Analysis
################################

# extract haloes from a cosmological simulation (returns snap.dat_sub000 and emmental.dat)
pExtract_Subhalo   --eps 0.1  --MhMax 1e7  --Mfrac 0.25 --MinMs 1e3  snap.dat

# extract physical properties
gExtractProperties --output analysis/ --center histocenter snap/snapshot_1169.hdf5 --inertial

################################
# Extraction from a reference halo
################################


# extraction from a reference snapshot (simple approach, multiple files)
gExtractFromRefSnapshot  --ref reference_snapshot.hdf5  -o output_directory  snap/snapshot_*

# extraction from a reference snapshot (simple approach, one single file)
gExtractFromRefSnapshot  --ref reference_snapshot.hdf5  -o snap.dat  snap/snapshot_0001




# extraction recursively from a reference snapshot (one single file)
gTraceBuildUp --tpot 2 -o outputfile.hdf5 --ref reference_halo.dat   snap/snapshot_0256

# extraction recursively from a reference snapshot (one single file with units conversion)
gTraceBuildUp --param1 params --param2 params.dSph --tpot 2 -o outputfile.dat --ref reference_halo.dat   snap/snapshot_0256

# extraction recursively from a reference snapshot (multiples files)
gTraceBuildUp --tpot 2 -o outdirectory --ref reference_halo.dat   snap/snapshot_*

# extraction recursively from a reference snapshot (multiples files with units conversion)
gTraceBuildUp --param1 params --param2 params.dSph --tpot 2 -o outdirectory --ref reference_halo.dat   snap/snapshot_*

# extraction recursively from a reference snapshot (multiples files with units conversion) with fixed radius
gTraceBuildUp  --tpot 2 --rvir 150 --frvir 0.3 --histocenter --ref reference_halo.dat   snap/snapshot_*

# restarting
gTraceBuildUp  --tpot 2 --rvir 150 --frvir 0.3 --histocenter --ref reference_halo.dat   snap/snapshot_* --restart





################################
# Compute centers and extract from a reference snapshots
################################

# compute the center of the structure that ends-up in halo_center.hdf5
gComputeCenters --ref halo_center.hdf5 -o centers.pkl snap/snapshot_*

# interpolate those centrers (save the centers as Nbody file : centers_tmp.gh5)
gInterpolateCenters halo_center.pkl --nbodyfile centers_tmp.gh5 -k 3 -s 100000 -o halo_center_smoothed.pkl

# extract a region of size Rmax around a list of center from a pkl file
gExtractFromCenters --Rmax 50 -o outputs/ halo_center_smoothed.pkl



################################
# Compute galaxy build-dup
################################

# this is an experimental script: it takes as input the list of centers and their corresponding snapshots (see the previous section)
# it creates one single output that contains specific regions (at any time) relative to the central objects 
# this allows to show how galaxies form
gComputeBuildUp  --output_snapshot snap.hdf5 halo_center_smoothed.pkl




################################
# Generate and visualize palettes
################################

# example of parameters
r = np.array([0,  255.])/255.
g = np.array([0,  254.])/255.
b = np.array([0,  208.])/255.
q = -1.0



# generate a palette
gmkpalette --param stars.pal  -o stars

# visualize it
gplot_palette -p ./stars


################################
# Jeans Informations
################################

gJeans --Density 1e3 --Temperature 10
pJeansTemperature  --xmin -7 --xmax 7 --ymin 1 --ymax 7 --ParticleMass 4000   --NJeans 10



################################
# misc
################################

# add gas to a cosmological box

addgas


# gas fraction (in dev)
pMassGasvsTime --param params --center histocenter --legend --legend_txt current -p test.png -o out.pickle -a in.pickle --multiprocessing 4 snapshot*



# find and extract sub-haloes

gSubFind --DesNumNgb 200 --Softening 0.015 --Nmin 100 --subHaloesDirectory  SubHaloDir --outputAllSubHaloes allSub.dat --outputHaloCleaned cleaned.dat snap.dat



"""

outer = """
###################
# Outer Potential
###################

# In the final output, a few parameters will be given and need to be copied to the params file

# Tidal striping without periodic box (boxsize=-1 works only with gas=0), put the galaxy at the right position
gCosmoOrb --dir MilkyWay --ra 150 --rp 60 --r_pos 85 --rvsign -1 --gas 0 -o snap.dat --rdwarf 25 --boxsize -1 --param params --orbit orbit.p ../no_cosmo_orb.dat

# Tidal striping with a periodic box
gCosmoOrb --dir MilkyWay --ra 150 --rp 60 --r_pos 85 --rvsign -1 --gas 0 -o snap.dat --rdwarf 25 --boxsize 50 --param params --orbit orbit.p ../no_cosmo_orb.dat

# Ram pressure striping without initial halo particles (add a glass of static dm to the snapshot for gear)
gCosmoOrb --dir MilkyWay --ra 150 --rp 60 --r_pos 85 --rvsign -1 --gas 0 -o snap.dat --rdwarf 25 --boxsize 50 --param params --orbit orbit.p --add_glass ../no_cosmo_orb.dat

# Tidal + ram pressure striping
gCosmoOrb --dir MilkyWay --ra 150 --rp 60 --r_pos 85 --rvsign -1 --gas 1 -o snap.dat --rdwarf 25 --boxsize 50 --param params --orbit orbit.p --add_glass ../no_cosmo_orb.dat

# if you wish to remove the halo's gas from inside the galaxy, you can add the --remove_gas option

################################
# Outer Potential analysis
################################

# Check orbit (orbit.p is an optional output of gCosmoOrb)
pTrace -compare orbit.p -adhoc 1 snap/trace.txt

# Compare potentials
pOuterPotential --legend --legend_loc='lower right'  LouiseDir MilkyWay

# Generate an evolving potential from a current potential and an evolving potential from simulation
gGenerateEvolvingPotential -e LouiseDir -o Test MilkyWayConst

# Kelvin Helmholtz limits
pKelvinHelmholtzResolution -u 100 --log xy --legend snap.dat cold.num hot.num
"""



mist = """
################################
# isochrones,CMD based on MIST database
################################

# access the python database through python. The tatabase will be named DB
mist_py

# plot isochrones
mist_isochrones --Age 13000 --Fe -2 -1

"""



help_txt = """
Possible Options
    -p, --pscript  Show pscript commands
    -g, --gscript  Show gscript commands
    -s, --stat     Show stat commands
    --outer        Show outer potential commands
    -h, --help     Show this message
"""


def getText(argv):
    nber_arg_done = 1  # first one is 'ghelp'
    no_arg = True
    text = ""
    if "-g" in argv or "-gscript" in argv:
        no_arg = False
        nber_arg_done += 1
        text = text + gscript + "\n"

    if "-s" in argv or "-stat" in argv:
        no_arg = False
        nber_arg_done += 1
        text = text + stat + "\n"

    if "-p" in argv or "-pscript" in argv:
        no_arg = False
        nber_arg_done += 1
        text = text + pscript + "\n"

    if "--outer" in argv:
        no_arg = False
        nber_arg_done += 1
        text = text + outer + "\n"

    if "--mist" in argv:
        no_arg = False
        nber_arg_done += 1
        text = text + mist + "\n"

    if no_arg:
        text = gscript + "\n"
        text = text + stat + "\n"
        text = text + pscript + "\n"
        text = text + outer + "\n"
        text = text + mist + "\n"

    if nber_arg_done != len(argv):
        return None
    return text


def checkHelp(argv):
    if "-h" in argv or "--help" in argv:
        print(help_txt)
        exit()


if __name__ == "__main__":
    checkHelp(argv)

    text = getText(argv)

    if text is None:
        print(help_txt)
        exit()

    print(text)
