#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      gSubFind
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################

from mpi4py import MPI

from pNbody import *

import numpy as np
from PySubFind import gadget

import Ptools as pt

import shutil
import glob
import copy

from optparse import OptionParser


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_ftype_options(parser)

    parser.add_option("--DesNumNgb",
                      action="store",
                      type="float",
                      dest="DesNumNgb",
                      default=50,
                      help="DesNumNgb")

    parser.add_option("--MaxNumNgbDeviation",
                      action="store",
                      type="float",
                      dest="MaxNumNgbDeviation",
                      default=1,
                      help="MaxNumNgbDeviation")

    parser.add_option("--Softening",
                      action="store",
                      type="float",
                      dest="Softening",
                      default=0.015,
                      help="Softening")

    parser.add_option("--subHaloesDirectory",
                      action="store",
                      type="string",
                      dest="subHaloesDirectory",
                      default=None,
                      help="sub Haloes directory")

    parser.add_option("--outputAllSubHaloes",
                      action="store",
                      type="string",
                      dest="outputAllSubHaloes",
                      default=None,
                      help="file name for all halos merged together")

    parser.add_option("--outputHaloCleaned",
                      action="store",
                      type="string",
                      dest="outputHaloCleaned",
                      default=None,
                      help="file name of the final output : cleanded halo")

    parser.add_option("--outputLocalMaximums",
                      action="store",
                      type="string",
                      dest="outputLocalMaximums",
                      default=None,
                      help="save local Maximums as pNbody file")

    parser.add_option("--Nmin",
                      action="store",
                      type="int",
                      dest="Nmin",
                      default=10,
                      help="Nmin")

    (options, args) = parser.parse_args()

    pt.check_files_number(args)

    files = args

    return files, options

########################################################################
# additional routines
########################################################################


def save_subgroup(nb, i, iname, directory):

    idx = nb.num[i]
    nb = nb.selectc(nb.head == i)

    #name = os.path.join(directory,"sgroup%08d_%08d_%08d.dat"%(i,idx,iname))
    name = os.path.join(
        directory, "sgroup%08d_%08d_%08d.dat" %
        (iname, i, idx))
    nb.nzero = None
    nb.massarr = None
    nb.rename(name)
    nb.write()


########################################################################
# MAIN
########################################################################

def MakePlot(files, opt):
    file = files[0]


    ####################################
    # create/remove directories


    if opt.subHaloesDirectory:

        if os.path.isdir(opt.subHaloesDirectory):
            # remove the content
            shutil.rmtree(opt.subHaloesDirectory)

        # create the directory
        os.mkdir(opt.subHaloesDirectory)


    ####################################
    # open the nbody model

    nb = Nbody(file, ftype=opt.ftype)
    #nb = nb.select(2)

    # for new ids if we want to recovers the tpes later on
    #nb.num = np.arange(nb.nbody)

    # set all to gas                # !!! here, we loose the type
    nb.set_tpe(0)


    gadget.InitMPI()			# init MPI
    gadget.InitDefaultParameters()		# init default parameters


    params = {}

    params['DesNumNgb'] = opt.DesNumNgb
    params['MaxNumNgbDeviation'] = opt.MaxNumNgbDeviation

    params['SofteningGas'] = opt.Softening
    params['SofteningHalo'] = opt.Softening
    params['SofteningDisk'] = opt.Softening
    params['SofteningBulge'] = opt.Softening
    params['SofteningStars'] = opt.Softening
    params['SofteningBndry'] = opt.Softening
    params['SofteningGasMaxPhys'] = opt.Softening
    params['SofteningHaloMaxPhys'] = opt.Softening
    params['SofteningDiskMaxPhys'] = opt.Softening
    params['SofteningBulgeMaxPhys'] = opt.Softening
    params['SofteningStarsMaxPhys'] = opt.Softening
    params['SofteningBndryMaxPhys'] = opt.Softening


    gadget.SetParameters(params)
    params = gadget.GetParameters()

    gadget.LoadParticles(np.array(nb.npart), nb.pos, nb.vel, nb.mass, nb.num, nb.tpe)
    gadget.hydro_force()


    # get properties
    nb.pos = gadget.GetAllPositions()
    nb.vel = gadget.GetAllVelocities()
    nb.mass = gadget.GetAllMasses()
    nb.num = gadget.GetAllID()
    nb.rho = gadget.GetAllDensities()
    nb.rsp = gadget.GetAllHsml()
    nb.u = gadget.GetAllEnergySpec()


    # compute the first and second densest nearest neighbour (particles ids)
    nb.firstnearest = gadget.GetAllFirstNearest()
    nb.secondnearest = gadget.GetAllSecondNearest()


    # get old tpe at the right place : if we have redefined the ids (see above)
    #nb.tpe = tpes[nb.num]


    ####################################
    # save local maximum

    if opt.outputLocalMaximums:

        nblm = nb.selectc((nb.firstnearest == -1) * (nb.secondnearest == -1))
        nblm.rename(opt.outputLocalMaximums)
        nblm.write()


    ####################################
    # now perform subfind

    print()
    print("Starting the subfind algorithm")
    print()

    # sort particles according to their density
    nb = nb.sort(1.0 / nb.rho)

    # init some particle properties to creat groups
    # at the beginning, each particle is a group with one particle

    nb.head = np.arange(nb.nbody)              # head particle in a group
    nb.tail = np.arange(nb.nbody)              # tail particle in a group
    nb.next = -1 * np.ones(nb.nbody, int)        # next particle in a group
    nb.len = np.ones(nb.nbody, int)

    nb.saved = -1 * np.ones(nb.nbody, int)

    iname = 0
    irhomax = 0


    # loop over all particles
    for i in range(nb.nbody):

        if np.fmod(i, 10**int(np.log10(nb.nbody) - 1)) == 0:
            print(("%d/%d" % (i, nb.nbody)))

        # case 1 :
        # particle has no neigboring particles
        # create a new groups

        if nb.firstnearest[i] == -1 and nb.secondnearest[i] == -1:
            # do nothing (it is a group by default)
            continue

        # case 2a :
        # particle has only one neighboring particle
        # attach it to the same group

        if nb.firstnearest[i] != -1 and nb.secondnearest[i] == -1:
            # attach the particle to the group
            j = nb.getindex(nb.firstnearest[i])

            nb.next[nb.tail[nb.head[j]]] = i
            nb.tail[nb.head[j]] = i
            nb.len[nb.head[j]] = nb.len[nb.head[j]] + 1
            nb.head[i] = nb.head[j]
            continue

        if nb.firstnearest[i] == -1 and nb.secondnearest[i] != -1:
            # attach the particle to the group
            j = nb.getindex(nb.secondnearest[i])

            nb.next[nb.tail[nb.head[j]]] = i
            nb.tail[nb.head[j]] = i
            nb.len[nb.head[j]] = nb.len[nb.head[j]] + 1
            nb.head[i] = nb.head[j]
            continue

        # here , the particle have two valid neighboring particles
        j = nb.getindex(nb.firstnearest[i])
        k = nb.getindex(nb.secondnearest[i])

        # case 2b :
        # the two neighbouring particles belongs to the same group

        if nb.head[j] == nb.head[k]:
            # attach the particle to the group

            nb.next[nb.tail[nb.head[j]]] = i
            nb.tail[nb.head[j]] = i
            nb.len[nb.head[j]] = nb.len[nb.head[j]] + 1
            nb.head[i] = nb.head[j]
            continue

        # case 3 :
        # the two neighbouring particles belongs to different groups

        # here, we have a two subhaloes candidates
        # save them (exept the densest one)

        if opt.subHaloesDirectory:

            if nb.len[nb.head[k]] > nb.len[nb.head[j]]:
                if (nb.len[nb.head[j]] > opt.Nmin):
                    save_subgroup(nb, nb.head[j], iname, opt.subHaloesDirectory)
                    iname = iname + 1
            else:
                if (nb.len[nb.head[k]] > opt.Nmin):
                    save_subgroup(nb, nb.head[k], iname, opt.subHaloesDirectory)
                    iname = iname + 1


        # if opt.subHaloesDirectory:
        #
        # #if nb.saved[nb.head[j]]==-1 and nb.head[j]!=irhomax:
        # #if nb.saved[nb.head[j]]==-1:
        # nb.saved[nb.head[j]] = 1
        # save_subgroup(nb,nb.head[j],iname,opt.subHaloesDirectory)
        # iname = iname+1
        #
        #
        # #if nb.saved[nb.head[k]]==-1 and nb.head[k]!=irhomax:
        # #if nb.saved[nb.head[k]]==-1:
        # nb.saved[nb.head[k]] = 1
        # save_subgroup(nb,nb.head[k],iname,opt.subHaloesDirectory)
        # iname = iname+1
        #

        # now merge the two subgroups

        if nb.len[nb.head[k]] > nb.len[nb.head[j]]:
            # particle k is in a group larger than particle j
            
            nb.next[nb.tail[nb.head[k]]] = nb.head[j]
            nb.tail[nb.head[k]] = nb.tail[nb.head[j]]
            nb.len[nb.head[k]] = nb.len[nb.head[k]] + nb.len[nb.head[j]]

            # loop over particles of the shortest group and change the head
            jj = nb.head[j]
            while(jj > -1):
                nb.head[jj] = nb.head[k]
                jj = nb.next[jj]

        else:                                           # particle j is in a group larger than particle k

            nb.next[nb.tail[nb.head[j]]] = nb.head[k]
            nb.tail[nb.head[j]] = nb.tail[nb.head[k]]
            nb.len[nb.head[j]] = nb.len[nb.head[j]] + nb.len[nb.head[k]]

            # loop over particles of the shortest group and change the head
            ii = nb.head[k]
            while(ii > -1):
                nb.head[ii] = nb.head[j]
                ii = nb.next[ii]

        # and add the new particle  to the merged group

        nb.next[nb.tail[nb.head[j]]] = i
        nb.tail[nb.head[j]] = i
        nb.len[nb.head[j]] = nb.len[nb.head[j]] + 1
        nb.head[i] = nb.head[j]
        continue


    ####################################
    # eventually sort groups


    #nb  = nb.selectc(nb.head==18)
    # nb.rename("qq.dat")
    # nb.write()


    ####################################
    # now group all haloes

    print()
    print("Merge all subhaloes")
    print()

    haloes = glob.glob(os.path.join(opt.subHaloesDirectory, "*"))

    nbtot = None

    for halo in haloes:

        nb = Nbody(halo, ftype="gadget")

        if nbtot is None:
            nbtot = copy.deepcopy(nb)
        else:
            nbtot.append(nb, do_not_change_num=True)


    if opt.outputAllSubHaloes:
        nbtot.rename(opt.outputAllSubHaloes)
        nbtot.write()


    ####################################
    # now group all haloes


    print()
    print("Remove sub haloes from the original file")
    print()


    nb = Nbody(file, ftype=opt.ftype)


    nb = nb.selectp(lst=nbtot.num, reject=True)


    if opt.outputHaloCleaned:
        nb.rename(opt.outputHaloCleaned)
        nb.write()



if __name__ == "__main__":
    files, opt = parse_options()

    MakePlot(files, opt)
