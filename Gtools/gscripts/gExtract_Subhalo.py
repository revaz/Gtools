#!/astro/soft/common/gcc/5.4.0/python/3.7.1/bin/python3

from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
from pNbody import ctes
import sys


def parse_options():
	usage = "usage: %prog [options] file"
	parser = OptionParser(usage=usage)

	parser = pt.add_ftype_options(parser)
	parse  = pt.add_units_options(parser)

	parser.add_option("-o",
  		    action="store", 
  		    dest="output",
  		    type="string",
		    default = None,		    
  		    help="output file name",       
  		    metavar=" STRING")

	parser.add_option("--eps",
  		    action="store", 
  		    dest="eps",
  		    type="float",
		    default = 0.1,		    
  		    help="grav. softening length",       
  		    metavar=" FLOAT")

	parser.add_option("--MhMax",
  		    action="store", 
  		    dest="MhMax",
  		    type="float",
		    default = 1e7,		    
  		    help="Max halo mass in Msol",       
  		    metavar=" FLOAT") 
		    
	parser.add_option("--Mfrac",
  		    action="store", 
  		    dest="Mfrac",
  		    type="float",
		    default = 0.25,		    
  		    help="Maximum mass fraction of polluted particles",       
  		    metavar=" FLOAT") 
		 
		 
	parser.add_option("--MinMs",
  		    action="store", 
  		    dest="MinMs",
  		    type="float",
		    default = 1e3,		    
  		    help="Minimum stellar mass",       
  		    metavar=" FLOAT") 
		 
	
	
	parser.add_option("--output_directory",
  		    action="store", 
  		    dest="output_directory",
  		    type="string",
		    default = '.',		    
  		    help="output directory name",       
  		    metavar=" STRING") 



	(options, args) = parser.parse_args()
	files = args
	return files,options
	

#######################################
# main
#######################################
dic = {}
files,opt = parse_options()

file1 = sys.argv[1]

nb = Nbody(file1, ftype = 'gadget')
output_file = file1


################
# units
################ 

# define local units
unit_params = pt.do_units_options(opt)
nb.set_local_system_of_units(params=unit_params)


out_units = units.UnitSystem('local',[units.Unit_kpc,units.Unit_Msol,units.Unit_s,units.Unit_K])
fM = nb.localsystem_of_units.convertionFactorTo(out_units.UnitMass)
HubbleParam = nb.hubbleparam


i = 0
while 1:
	mdict = {}
	print(30*"#")
	print("still working on : ", file1)

	#select DM only
	nbh = nb.select(2)

	nbh.pot = nbh.TreePot(nbh.pos,opt.eps)
	offset = -nbh.pos[argmin(nbh.pot)]

	#center the halo
	nb.translate(offset)


	try:
		R200,M200 = nb.GetVirialRadius(inCodeUnits=True)
	except:
		print("!!!! problem when computing R200...")
		nb.rename("/tmp/qq.dat")
		nb.write()
		break
    
	nbsub = nb.selectc(nb.rxyz()<=R200)
    
	Mh = nbsub.mass_tot * fM/HubbleParam
    
	if Mh < opt.MhMax:
		print("breaking")
		break
      
	if output_file!=None:
    
		nbs = nbsub.select(1)
		Ms = nbs.mass_tot * fM/HubbleParam
    
		if  Ms > opt.MinMs:					  # keep only if the stellar content is larger than...
    
			nbbnd = nbsub.select(5)
     
			Mfrac = nbbnd.mass_tot/nbsub.mass_tot
			if (Mfrac) < opt.Mfrac: 	  # keep only if not too polluted
      
  	  			# save halo
  	  			#name,ext = os.path.splitext(output_file)
				name = output_file
				ext=""

				output = name + "_sub%03d"%i + ext
				bname = os.path.basename(name + "_sub%03d"%i)
				nbsub.rename(output)
				nbsub.write()


				mdict['name']                  = bname
				mdict['subnumber']             = i
				mdict['atime']                 = nb.atime
				mdict['RawHaloOutput']         = output
				mdict['RawHaloPos']            = -offset
				mdict['RawHaloContamination']  = Mfrac
				mdict['RawHaloMass']           = nbsub.mass_tot


				dic[bname] = mdict
				print("done : %s"%(output))
				i = i+1


			else:
				print("too poluted !!! skipping...",  nbbnd.mass_tot/nbsub.mass_tot)
	else:
		print("stellar mass is too small !!! skipping...")

	#keep only the rest
	nb = nb.selectc(nb.rxyz()>R200)
	#translate back
	nb.translate(-offset)


	nb.rename("emmental.dat")
	nb.write()



