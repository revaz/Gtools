#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      pExtractProperties
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################


from optparse import OptionParser
import Ptools as pt
import os


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)
    parser = pt.add_center_options(parser)

    parser.add_option("--output_directory",
                      action="store",
                      dest="output_directory",
                      type="string",
                      default='.',
                      help="output directory name",
                      metavar=" STRING")

    (options, args) = parser.parse_args()

    pt.check_files_number(args)

    files = args

    return files, options


def execute_command(com):
    exit_code = os.system(com)
    if exit_code != 0:
        raise SystemError(
            "Command '%s' failed with exit code %i" %
            (com, exit_code))

#######################################
# Main
#######################################


files, opt = parse_options()

if not os.path.isdir(opt.output_directory):
    os.makedirs(opt.output_directory)

for file in files:

    print(50 * "#")
    print(file)

    udsph_snap = file

    halo_name = os.path.basename(udsph_snap)
    bout = os.path.join(opt.output_directory, halo_name)

    print(bout)

    out_ALL = "%s_%s.png" % (bout, "ALL")
    out_ALLcomp = "%s_%s.png" % (bout, "ALL_Comp")
    out_ALLALL = "%s_%s.png" % (bout, "ALLALL")

    center = "--center %s" % opt.center
    if opt.center is None:
        center = ""
    inertial = ""
    if opt.inertial:
        inertial = "--inertial"
    ###############################################
    # 2) star formation rates

    print("#" * 10 + " Computing Star Formation Rate " + "#" * 10)
    out_SFR = "%s_%s.png" % (bout, "SFR")
    com = "pSFRvsTime %s --param params -o SFR  --xmin=0 --xmax=14 --ymin 0 --ymax 0.008 %s             -p %s" % (
        center, udsph_snap, out_SFR)
    execute_command(com)

    ###############################################
    # 3) N vs Fe

    print("#" * 16 + " Computing N vs Fe " + "#" * 17)

    out_FEH = "%s_%s.png" % (bout, "FEH")
    com = "pNvsX %s --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15 %s -p %s" % (
        center, udsph_snap, out_FEH)
    execute_command(com)

    out_FEHfnx = "%s_%s.png" % (bout, "FEH_Fnx")
    com = "pNvsX %s --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15 --data fornax   %s -p %s" % (
        center, udsph_snap, out_FEHfnx)
    execute_command(com)

    out_FEHscl = "%s_%s.png" % (bout, "FEH_Scl")
    com = "pNvsX %s --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15 --data sculptor %s -p %s" % (
        center, udsph_snap, out_FEHscl)
    execute_command(com)

    out_FEHsex = "%s_%s.png" % (bout, "FEH_Sex")
    com = "pNvsX %s --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15 --data sextans  %s -p %s" % (
        center, udsph_snap, out_FEHsex)
    execute_command(com)

    out_FEHcar = "%s_%s.png" % (bout, "FEH_Car")
    com = "pNvsX %s --param params --select stars1  --x Fe  --nx 40 --xmin=-4 --xmax=0.5 --ymin=0  --ymax=0.15 --data carina   %s -p %s" % (
        center, udsph_snap, out_FEHcar)
    execute_command(com)

    ###############################################
    # 4) MgFe vs FeH

    print("#" * 14 + " Computing MgFe vs FeH " + "#" * 15)

    out_MGFE = "%s_%s.png" % (bout, "MGFE")
    com = "pMgFevsFe %s --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5  %s                  -p %s" % (
        center, udsph_snap, out_MGFE)
    execute_command(com)

    out_MGFEfnx = "%s_%s.png" % (bout, "MGFE_Fnx")
    com = "pMgFevsFe %s --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5  --data fornax %s                    -p %s" % (
        center, udsph_snap, out_MGFEfnx)
    execute_command(com)

    out_MGFEscl = "%s_%s.png" % (bout, "MGFE_Scl")
    com = "pMgFevsFe %s --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5  --data sculptor %s                  -p %s" % (
        center, udsph_snap, out_MGFEscl)
    execute_command(com)

    out_MGFEsex = "%s_%s.png" % (bout, "MGFE_Sex")
    com = "pMgFevsFe %s --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5  --data sextans %s                   -p %s" % (
        center, udsph_snap, out_MGFEsex)
    execute_command(com)

    out_MGFEcar = "%s_%s.png" % (bout, "MGFE_Car")
    com = "pMgFevsFe %s --select=stars1  --xmin=-4 --xmax=0.5 --ymin=-1 --ymax=1.5  --data carina %s                    -p %s" % (
        center, udsph_snap, out_MGFEcar)
    execute_command(com)

    ###############################################
    # 5) vitesse circulaire et disp. de vitesses

    print("#" * 14 + " Computing Velocities " + "#" * 14)
    out_VEL = "%s_%s.png" % (bout, "VEL")
    com = "pplot_Velocities %s %s --xmax 10 --rmax 10 --nr 64  --ymin 0 --ymax 25  --param params  %s      -p %s" % (
        center, inertial, udsph_snap, out_VEL)
    execute_command(com)

    ###############################################
    # 6) luminosity profile

    print("#" * 14 + " Computing Luminosity " + "#" * 14)
    out_LUM = "%s_%s.png" % (bout, "LUM")
    com = "pLuminosityProfile %s --log y  --xmax 10 --rmax 10 --nr 64 --ymin 1e-5 --ymax 1e-1 --param params %s -p %s" % (
        center, udsph_snap, out_LUM)
    execute_command(com)

    ###############################################
    # 7) T-rho

    print("#" * 16 + " Computing Rho-T " + "#" * 17)
    out_TRHO = "%s_%s.png" % (bout, "TRHO")
    com = "pplot --param params  --select=gas --x logrho --y logT  --xmin -7 --xmax 7 --ymin 1 --ymax 7 %s -p %s" % (
        udsph_snap, out_TRHO)
    execute_command(com)

    ###############################################
    # 8) info

    print("#" * 17 + " Computing Info " + "#" * 17)
    out_INFO = "%s_%s.o" % (bout, "INFO")
    com = "pHaloInfo %s %s --param params %s -o %s" % (
        center, inertial, udsph_snap, out_INFO)
    execute_command(com)

    ###############################################
    # 9) MsvsMh

    print("#" * 15 + " Computing Ms vs Mh " + "#" * 15)
    out_MSVSMH = "%s_%s.png" % (bout, "MSVSMH")
    com = "pMsvsMh --legend %s -p %s" % (out_INFO, out_MSVSMH)
    execute_command(com)

    ###############################################
    # 10) convert png

    print("#" * 19 + " Converting " + "#" * 19)
    com = "convert +append  %s %s %s 1.png" % (out_MSVSMH, out_FEH, out_MGFE)
    execute_command(com)
    com = "convert +append  %s %s %s 2.png" % (out_SFR, out_VEL, out_LUM)
    execute_command(com)

    com = "convert -append 1.png 2.png  %s" % (out_ALL)
    execute_command(com)

    ###############################################
    # 11) comp with dsph

    com = "convert +append  %s %s %s %s 1.png" % (
        out_FEHfnx, out_FEHscl, out_FEHsex, out_FEHcar)
    execute_command(com)
    com = "convert +append  %s %s %s %s 2.png" % (
        out_MGFEfnx, out_MGFEscl, out_MGFEsex, out_MGFEcar)
    execute_command(com)

    com = "convert -append 1.png 2.png  %s" % (out_ALLcomp)
    execute_command(com)

    com = "convert +append  %s %s %s" % (out_ALL, out_ALLcomp, out_ALLALL)
    execute_command(com)

    print("#" * 18 + " Anaylsis Done" + "#" * 18)
