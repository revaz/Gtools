#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      gheader
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################
import numpy as np
import sys

from optparse import OptionParser
from h5py import File

from pNbody import iofunc


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser.add_option("-t", "--ftype",
                      action="store",
                      dest="ftype",
                      type="string",
                      default="gh5",
                      help="type of the file",
                      metavar=" TYPE")

    parser.add_option("--pollution_ratio",
                      action="store_true",
                      dest="pol_ratio",
                      default=False,
                      help="Print only the pollution ratio of the simulation")

    options, args = parser.parse_args()

    if len(args) == 0:
        files = None
    else:
        files = args

    return files, options


class Header:
    def __init__(self, filename, ftype, opt):
        self.filename = filename
        self.opt = opt
        if ftype == "gadget":
            self.read_gadget()
        elif ftype == "gh5":
            self.read_gh5()

    def read_gh5(self):
        f = File(self.filename)
        h = f["Header"].attrs

        self.npart = h["NumPart_ThisFile"]
        self.atime = h["Time"]
        self.redshift = h["Redshift"]
        self.nall = h["NumPart_Total"]
        self.boxsize = h["BoxSize"]
        self.omega0 = h["Omega0"]
        self.omegalambda = h["OmegaLambda"]
        self.hubbleparam = h["HubbleParam"]
        self.nallhw = h["NumPart_Total_HighWord"]

        self.nbody = sum(self.npart)

    def read_gadget(self):
        opt.pio = 'no'
        opt.byteorder = sys.byteorder
        tpl = (24, 48, float, float, np.int32, np.int32, 24, np.int32,
               np.int32, float, float, float, float, np.int32,
               np.int32, 24, np.int32, np.int32, float, 48)
        f = open(self.filename, 'rb')
        header = iofunc.ReadBlock(f, tpl, byteorder=opt.byteorder,
                                    pio=opt.pio)
        f.close()

        self.npart, self.massarr, self.atime, self.redshift,  \
            self.flag_sfr, self.flag_feedback, self.nall, \
            self.flag_cooling, self.num_files, self.boxsize, self.omega0, \
            self.omegalambda, self.hubbleparam, self.flag_age, \
            self.flag_metals, self.nallhw, self.flag_entr_ic, \
            self.flag_chimie_extraheader, self.critical_energy_spec, \
            empty = header

        self.npart = np.frombuffer(self.npart, np.int32)
        self.massarr = np.frombuffer(self.massarr, float)
        self.nall = np.frombuffer(self.nall, np.int32)
        self.nallhw = np.frombuffer(self.nallhw, np.int32)

        self.nbody = sum(self.npart)

    def __str__(self):
        txt = "%s: atime=%13.10f, redshift=%13.10f, nbody=%8d, " % (
            self.filename, self.atime, self.redshift, self.nbody)
        txt += "npart=" + str(self.npart)
        txt += ", omega0=%1.3f, omegaL=%1.3f, H0=%1.3f" % (
            self.omega0, self.omegalambda, self.hubbleparam)
        return txt

    def print_pollution_ratio(self):
        print(("{}: Pollution {:.3%}".format(
            self.filename, float(sum(self.npart[3:6])) / self.nbody)))

# ##############################################################################
#
#                                    MAIN
#
# ##############################################################################


def MakePlot(files, opt):
    for f in files:
        h = Header(f, opt.ftype, opt)

        if opt.pol_ratio:
            h.print_pollution_ratio()
        else:
            print(h)


if __name__ == "__main__":
    files, opt = parse_options()

    MakePlot(files, opt)
