#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      pplot_Velocities
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################


from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
from pNbody import ctes
from pNbody import libgrid, cosmo


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_postscript_options(parser)
    parser = pt.add_ftype_options(parser)
    parser = pt.add_reduc_options(parser)
    parser = pt.add_center_options(parser)
    parser = pt.add_select_options(parser)
    parser = pt.add_cmd_options(parser)
    parser = pt.add_display_options(parser)
    parser = pt.add_info_options(parser)
    parser = pt.add_limits_options(parser)
    parser = pt.add_log_options(parser)
    parser = pt.add_units_options(parser)
    parser = pt.add_legend_options(parser)

    parser.add_option("--rmax",
                      action="store",
                      dest="rmax",
                      type="float",
                      default=50.,
                      help="max radius of bins",
                      metavar=" FLOAT")

    parser.add_option("--nr",
                      action="store",
                      dest="nr",
                      type="int",
                      default=32,
                      help="number of bins in r",
                      metavar=" INT")

    parser.add_option("--forceComovingIntegrationOn",
                      action="store_true",
                      dest="forceComovingIntegrationOn",
                      default=False,
                      help="force the model to be in in comoving integration")

    parser.add_option("--curve",
                      action="store_true",
                      dest="curve",
                      default=False,
                      help="draw curve instead of histogram")

    parser.add_option("-o",
                      action="store",
                      type="string",
                      dest="o",
                      default='density',
                      help="value to plot")

    parser.add_option("--AdaptativeSoftenning",
                      action="store_true",
                      dest="AdaptativeSoftenning",
                      default=False,
                      help="AdaptativeSoftenning")

    parser.add_option("--ErrTolTheta",
                      action="store",
                      dest="ErrTolTheta",
                      type="float",
                      default=0.5,
                      help="Error tolerance theta",
                      metavar=" FLOAT")

    parser.add_option("--eps",
                      action="store",
                      dest="eps",
                      type="float",
                      default=0.1,
                      help="smoothing length",
                      metavar=" FLOAT")

    (options, args) = parser.parse_args()

    pt.check_files_number(args)

    files = args

    return files, options


#######################################
# MakePlot
#######################################


def MakePlot(files, opt):

    # some inits
    colors = pt.Colors(n=len(files))
    datas = []

    # read files
    for file in files:

        nb = Nbody(file, ftype=opt.ftype)

        ################
        # units
        ################

        # define local units
        unit_params = pt.do_units_options(opt)
        nb.set_local_system_of_units(params=unit_params)

        # define output units
        # nb.ToPhysicalUnits()

        if opt.forceComovingIntegrationOn:
            nb.setComovingIntegrationOn()

        ################
        # apply options
        ################
        nb = pt.do_reduc_options(nb, opt)
        nb = pt.do_select_options(nb, opt)
        nb = pt.do_center_options(nb, opt)
        nb = pt.do_cmd_options(nb, opt)
        nb = pt.do_info_options(nb, opt)
        nb = pt.do_display_options(nb, opt)

        ################
        # some info
        ################
        print("---------------------------------------------------------")
        nb.localsystem_of_units.info()
        nb.ComovingIntegrationInfo()
        print("---------------------------------------------------------")

        # grid division
        rc = 1

        def f(r): return np.log(r / rc + 1.)

        def fm(r): return rc * (np.exp(r) - 1.)

        ###############################
        # compute physical quantities
        ###############################

        nbs = nb.select('stars1')

        ##########################################
        # Spherical_1d_Grid
        ##########################################

        #######################
        # circular velocity

        Gcte = ctes.GRAVITY.into(nb.localsystem_of_units)

        G = libgrid.Spherical_1d_Grid(
            rmin=0, rmax=opt.rmax, nr=opt.nr, g=f, gm=fm)

        x = G.get_r()
        M = G.get_MassMap(nb)
        M = np.add.accumulate(M)

        y = np.sqrt(Gcte * M / x)

        # comoving conversion
        if nb.isComovingIntegrationOn():
            print("	 converting to physical units (a=%5.3f h=%5.3f)" % (nb.atime, nb.hubbleparam))
            x = x * nb.atime / nb.hubbleparam	      # length  conversion

        # output units
        out_units_x = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        out_units_y = units.UnitSystem(
            'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

        fx = nb.localsystem_of_units.convertionFactorTo(out_units_x.UnitLength)
        fy = nb.localsystem_of_units.convertionFactorTo(
            out_units_y.UnitVelocity)

        x = x * fx
        y = y * fy

        # set labels

        xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
        ylabel = r'$\rm{Velocities}\,\left[ \rm{km}/\rm{s} \right]$'

        x, y = pt.CleanVectorsForLogX(x, y)
        x, y = pt.CleanVectorsForLogY(x, y)
        datas.append(
            pt.DataPoints(
                x,
                y,
                color=colors.get(),
                label=file +
                ": circ",
                tpe='points'))

        #######################
        # velocity disp. in z (tot)

        # comoving conversion
        if nb.isComovingIntegrationOn():
            print("	 converting to physical units (a=%5.3f h=%5.3f)" % (nb.atime, nb.hubbleparam))

            x0 = nb.pos
            v0 = nb.vel

            Hubble = ctes.HUBBLE.into(nb.localsystem_of_units)
            pars = {
                "Hubble": Hubble,
                "HubbleParam": nb.hubbleparam,
                "OmegaLambda": nb.omegalambda,
                "Omega0": nb.omega0}
            a = nb.atime
            Ha = cosmo.Hubble_a(a, pars=pars)

            nb.pos = x0 * nb.atime / nb.hubbleparam     # length    conversion
            nb.vel = v0 * np.sqrt(a) + x0 * Ha * a	      # velocity  conversion

        # output units
        out_units_x = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        out_units_y = units.UnitSystem(
            'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

        fx = nb.localsystem_of_units.convertionFactorTo(out_units_x.UnitLength)
        fy = nb.localsystem_of_units.convertionFactorTo(
            out_units_y.UnitVelocity)

        nb.pos = nb.pos * fx
        nb.vel = nb.vel * fy

        #G = libgrid.Cylindrical_2drt_Grid(rmin=0,rmax=opt.rmax,nr=opt.nr,nt=1,g=f,gm=fm)
        #
        #x,t  = G.get_rt()
        #y  = G.get_SigmaValMap(nb,nb.Vz())
        #y = sum(y,axis=1)

        G = libgrid.Spherical_1d_Grid(
            rmin=0, rmax=opt.rmax, nr=opt.nr, g=f, gm=fm)
        x = G.get_r()
        #vx  = G.get_SigmaValMap(nb,nb.vx())
        #vy  = G.get_SigmaValMap(nb,nb.vy())
        #vz  = G.get_SigmaValMap(nb,nb.vz())
        #y = np.sqrt(vx**2+vy**2+vz**2)
        #y = G.get_SigmaValMap(nb,np.sqrt(nb.vx()**2 + nb.vy()**2 + nb.vz()**2))
        y = G.get_SigmaValMap(nb, nb.Vr())

        # set labels

        xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
        ylabel = r'$\rm{Velocities}\,\left[ \rm{km}/\rm{s} \right]$'

        x, y = pt.CleanVectorsForLogX(x, y)
        x, y = pt.CleanVectorsForLogY(x, y)
        datas.append(
            pt.DataPoints(
                x,
                y,
                color=colors.get(),
                label=file +
                ": v_z (all)",
                tpe='points',
                linestyle='-.'))

        #######################
        # velocity disp. in z (stars)

        # comoving conversion
        if nbs.isComovingIntegrationOn():
            print("	 converting to physical units (a=%5.3f h=%5.3f)" % (nbs.atime, nbs.hubbleparam))

            x0 = nbs.pos
            v0 = nbs.vel

            Hubble = ctes.HUBBLE.into(nbs.localsystem_of_units)
            pars = {
                "Hubble": Hubble,
                "HubbleParam": nbs.hubbleparam,
                "OmegaLambda": nbs.omegalambda,
                "Omega0": nbs.omega0}
            a = nbs.atime
            Ha = cosmo.Hubble_a(a, pars=pars)

            nbs.pos = x0 * nbs.atime / nbs.hubbleparam     # length    conversion
            nbs.vel = v0 * np.sqrt(a) + x0 * Ha * a	      # velocity  conversion

        # output units
        out_units_x = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])
        out_units_y = units.UnitSystem(
            'local', [units.Unit_km, units.Unit_Ms, units.Unit_s, units.Unit_K])

        fx = nbs.localsystem_of_units.convertionFactorTo(
            out_units_x.UnitLength)
        fy = nbs.localsystem_of_units.convertionFactorTo(
            out_units_y.UnitVelocity)

        nbs.pos = nbs.pos * fx
        nbs.vel = nbs.vel * fy

        G = libgrid.Cylindrical_2drt_Grid(
            rmin=0, rmax=opt.rmax, nr=opt.nr, nt=1, g=f, gm=fm)

        x, t = G.get_rt()
        y = G.get_SigmaValMap(nbs, nbs.Vz())
        y = sum(y, axis=1)

        # set labels

        xlabel = r'$\rm{Radius}\,\left[ \rm{kpc} \right]$'
        ylabel = r'$\rm{Velocities}\,\left[ \rm{km}/\rm{s} \right]$'

        x, y = pt.CleanVectorsForLogX(x, y)
        x, y = pt.CleanVectorsForLogY(x, y)
        datas.append(
            pt.DataPoints(
                x,
                y,
                color=colors.get(),
                label=file +
                ": v_z(stars)",
                tpe='points',
                linestyle='--'))

        nbs = nbs.selectc(nbs.rxy() < 1)
        vz = nbs.vel[:, 2]
        m = nbs.mass
        vzm = sum(vz * m) / sum(m)

        sigmavz2 = sum(m * vz**2) / sum(m) - vzm**2
        sigmavz = np.sqrt(sigmavz2)

        print(sigmavz)

    ##################
    # plot all
    ##################

    for d in datas:
        pt.plot(d.x, d.y, color=d.color, linestyle=d.linestyle)

    if opt.legend:
        pt.LegendFromDataPoints(datas)

    # set limits and draw axis
    xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(
        opt.xmin, opt.xmax, opt.ymin, opt.ymax, datas, opt.log)

    pt.SetAxis(xmin, xmax, ymin, ymax, opt.log)
    pt.xlabel(xlabel, fontsize=pt.labelfont)
    pt.ylabel(ylabel, fontsize=pt.labelfont)
    pt.grid(False)


########################################################################
# MAIN
########################################################################


if __name__ == '__main__':
    files, opt = parse_options()
    pt.InitPlot(files, opt)
    pt.pcolors
    MakePlot(files, opt)
    pt.EndPlot(files, opt)
