#!/usr/bin/env python3

from pNbody import Nbody
from sys import argv
from h5py import File
import numpy as np

if len(argv) != 3:
    raise Exception("You need to specify both the input and output filename")
filename = argv[-2]
output = argv[-1]

# number of particle types
ntype = 6


def groupName(part_type):
    """Generate the HDF5 group name"""
    return "PartType%i" % part_type


def changeType(f, old, new):
    """
    Move a HDF5 group to another name.

    Parameters
    ----------

    f: HDF5 file
      The file to modify

    old: int
      The particle type index of the group to move

    new: int
      The destination particle type index
    """
    # check if directory exists
    old_group = groupName(old)
    # check if the group exists
    if old_group not in f:
        raise IOError("Cannot find group '%s'" % old)
    old = f[old_group]

    new_group = groupName(new)
    # create the group if required
    if new_group not in f:
        f.create_group(new_group)
    new = f[new_group]

    # Move all the arrays to the new group
    for name in old:
        tmp = old[name][:]
        del old[name]
        if name in new:
            new_tmp = new[name][:]
            tmp = np.append(tmp, new_tmp, axis=0)
            del new[name]

        new.create_dataset(name, tmp.shape)
        new[name][:] = tmp

    del f[old_group]


def countPart(f):
    """
    Count the number of particles in a snapshot
    and update the required fields.
    """
    npart = []
    for i in range(ntype):
        name = groupName(i)
        if name in f:
            grp = f[groupName(i)]
            N = grp["Masses"].shape[0]
        else:
            N = 0
        npart.append(N)

    f["Header"].attrs["NumPart_ThisFile"] = npart
    f["Header"].attrs["NumPart_Total"] = npart
    f["Header"].attrs["NumPart_Total_HighWord"] = [0]*ntype
    f["Header"].attrs["Flag_Entropy_ICs"] = 0


###################
# Convert to HDF5 #
###################
print("Converting to HDF5")

# open file
nb = Nbody(filename)

# Compute the smoothing length
nb.rsp = nb.get_rsp_approximation()

# change file type and write it
nb = nb.set_ftype("swift")
nb.rename(output)
nb.write()

# cleanup memory
del nb

##########################
# Update the fields name #
##########################
f = File(output, "a")

# List of transformation
d = {
    "InternalEnergies": "InternalEnergy",
    "SmoothingLengths": "SmoothingLength"
}

# apply transformation to all particle types
print("Transform field name")
for i in range(ntype):
    for field in d:
        # Get the name of the group
        name = groupName(i)
        # New name
        link_name = name + "/" + d[field]
        # old name
        field_name = name + "/" + field
        # check if field is present
        if (link_name not in f and field_name in f):
            # create a link
            f[link_name] = f[field_name]

############################
# Change the particle type #
############################
print("Changing the particle types")
changeType(f, 2, 1)
# changeType(f, 3, 2)
# changeType(f, 4, 2)
changeType(f, 5, 2)

countPart(f)

f.close()
