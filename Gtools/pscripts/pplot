#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      pplot
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################


from optparse import OptionParser
import Ptools as pt
from pNbody import *
from pNbody import units
from pNbody import ctes
from pNbody import thermodyn

from scipy import optimize


def parse_options():

    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    options = [
        "postscript",
        "ftype",
        "reduc",
        "center",
        "select",
        "cmd",
        "display",
        "info",
        "limits",
        "log",
        "units",
        "comoving",
        "legend"
    ]

    parser.add_option("--x",
                      action="store",
                      dest="x",
                      type="string",
                      default='r',
                      help="x value to plot",
                      metavar=" STRING")

    parser.add_option("--y",
                      action="store",
                      dest="y",
                      type="string",
                      default='T',
                      help="y value to plot",
                      metavar=" STRING")

    parser.add_option("--z",
                      action="store",
                      dest="z",
                      type="string",
                      default=None,
                      help="z value to plot",
                      metavar=" STRING")

    parser.add_option("--colorbar",
                      action="store_true",
                      dest="colorbar",
                      default=False,
                      help="add a colorbar")

    parser.add_option("--nx",
                      action="store",
                      type="int",
                      dest="nx",
                      default=64,
                      help="number of bins in x")

    parser.add_option("--ny",
                      action="store",
                      type="int",
                      dest="ny",
                      default=64,
                      help="number of bins in y")

    parser.add_option("--nopoints",
                      action="store_true",
                      dest="nopoints",
                      default=False,
                      help="do not plot points")

    parser.add_option("--map",
                      action="store_true",
                      dest="map",
                      default=False,
                      help="plot map instead of points")

    parser.add_option("--accumulate",
                      action="store_true",
                      dest="accumulate",
                      default=False,
                      help="integrate histogramm")

    parser.add_option("--density",
                      action="store_true",
                      dest="density",
                      default=False,
                      help="compute density")

    parser.add_option("--data",
                      action="store",
                      type="string",
                      dest="data",
                      default=None,
                      help="data")

    parser.add_option("--mc",
                      action="store",
                      type="int",
                      dest="mc",
                      default=0,
                      help="number montecarlo point")

    parser.add_option("--PJeans",
                      action="store_true",
                      dest="PJeans",
                      default=False,
                      help="add Jeans Pressure Floor")

    parser.add_option("--JeansHsml",
                      action="store",
                      type="float",
                      dest="JeansHsml",
                      default=None,
                      help="resolution for the Jeans pressure floor")

    parser.add_option("--NJ",
                      action="store",
                      type="float",
                      dest="NJ",
                      default=10.,
                      help="Jeans factor for jeans pressure floor")

    parser.add_option("--Softening",
                      action="store",
                      type="float",
                      dest="Softening",
                      default=None,
                      help="Gravitational softening for the pressure floor")

    parser.add_option(
        "--SofteningMaxPhys",
        action="store",
        type="float",
        dest="SofteningMaxPhys",
        default=None,
        help="Gravitational softening (maxphys) for the pressure floor")

    files, options = pt.parse_options(parser=parser, options=options)

    return files, options


def get_value_and_label(nb, val, args=None):

    if val == 'R':
        label = r'$\rm{Radius}\,\left[ kpc \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Rxyz(units=out_units.UnitLength)
        return val, label

    if val == 'logR':
        label = r'$\rm{log Radius}\,\left[ kpc \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Rxyz(units=out_units.UnitLength)
        val = np.log10(val)

        return val, label

    if val == 'Rxy':
        label = r'$\rm{Radius}\,\left[ kpc \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Rxy(units=out_units.UnitLength)
        return val, label

    if val == 'Hsml':
        label = r'$\rm{SPH\,Smoothin\,Length}\,\left[ kpc \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.SphRadius(units=out_units.UnitLength)
        return val, label

    if val == 'logHsml':
        label = r'$\rm{log SPH\,Smoothin\,Length}\,\left[ kpc \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = np.log10(nb.SphRadius(units=out_units.UnitLength))
        return val, label

    elif val == 'T':
        label = r'$\rm{Temperature}\,\left[ \rm{K} \right]$'
        val = nb.T()
        return val, label

    elif val == 'logT':
        label = r'$\rm{log\,Temperature}\,\left[ \rm{K} \right]$'

        val = nb.T()
        val = np.log10(val)
        return val, label

    elif val == 'logTJeans':
        label = r'$\rm{log\,Jeans\,Temperature}\,\left[ \rm{K} \right]$'

        Hsml = None
        if args is not None:
            if "JeansHsml" in args:
                Hsml = args["JeansHsml"]

        Softening = None
        if args is not None:
            if "Softening" in args:
                Softening = args["Softening"]

        SofteningMaxPhys = None
        if args is not None:
            if "SofteningMaxPhys" in args:
                SofteningMaxPhys = args["SofteningMaxPhys"]

        val = nb.TJeans(
            Hsml=Hsml,
            Softening=Softening,
            SofteningMaxPhys=SofteningMaxPhys)
        val = np.log10(val)

        return val, label

    elif val == 'logResolvedSPHMass':
        label = r'$\rm{log\,Mass}\,\left[ \rm{Msol} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_cm, units.Unit_Msol, units.Unit_s, units.Unit_K])

        h = nb.SphRadius()
        rho = nb.Rho()

        val = 4. / 3. * np.pi * rho * h**3 * \
            nb.localsystem_of_units.convertionFactorTo(out_units.UnitMass)
        val = np.log10(val)

        return val, label

    elif val == 'rho':
        label = r'$\rm{Density}\,\left[ \rm{atom/cm^3} \right]$'

        Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
        Unit_atom.set_symbol('atom')
        out_units = units.UnitSystem(
            'local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])

        val = nb.Rho(units=out_units.UnitDensity)

        return val, label

    elif val == 'logrho':
        label = r'$\rm{log\,Density}\,\left[ \rm{atom/cm^3} \right]$'

        Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
        Unit_atom.set_symbol('atom')
        out_units = units.UnitSystem(
            'local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])

        val = nb.Rho(units=out_units.UnitDensity)
        val = np.log10(val)

        return val, label

    elif val == 'logFormationGasDensity':
        label = r'$\rm{log\,FormationGasDensity}\,\left[ \rm{atom/cm^3} \right]$'

        Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
        Unit_atom.set_symbol('atom')
        out_units = units.UnitSystem(
            'local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])

        val = nb.FormationGasDensity(units=out_units.UnitDensity)
        val = np.log10(val)

        return val, label


    elif val == 'logFormationGasTemperature':
        label = r'$\rm{log\,Temperature}\,\left[ \rm{K} \right]$'

        val = nb.Tstar
        val = np.log10(val)
        return val, label
        
        


    elif val == "logP":
        label = r"$\rm{log\,Pressure}\,\left[ \rm{M_\odot / kpc Myr^2} \right]$"
        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Pressure(units=out_units)
        val = np.log10(val)

        return val, label

    elif val == 'Tcool':
        label = r'$\rm{Cooling\,Time}\,\left[ \rm{Myr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Tcool(units=out_units.UnitTime)
        
        return val, label

    elif val == 'logTcool':
        label = r'$\rm{log\,Cooling\,Time}\,\left[ \rm{Myr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Tcool(units=out_units.UnitTime)
        val = np.log10(val)

        return val, label

    elif val == 'Tff':
        label = r'$\rm{Free\,Fall\,Time}\,\left[ \rm{Myr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Tff(units=out_units.UnitTime)

        return val, label

    elif val == 'logTff':
        label = r'$\rm{log\,Free\,Fall\,Time}\,\left[ \rm{Myr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Myr, units.Unit_K])

        val = nb.Tff(units=out_units.UnitTime)
        val = np.log10(val)

        return val, label

    elif val == 'Age':
        label = r'$\rm{Age}\,\left[ \rm{Gyr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Gyr, units.Unit_K])

        val = nb.StellarAge(units=out_units.UnitTime)

        return val, label
        
    elif val == 'tstar':
        label = r'$a$'

        #out_units = units.UnitSystem(
        #    'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Gyr, units.Unit_K])

        val = nb.tstar

        return val, label        
        
        

    elif val == 'CosmicTime':
        label = r'$\rm{Cosmic}\, \rm{Time}\, \left[ \rm{Gyr} \right]$'

        out_units = units.UnitSystem(
            'local', [units.Unit_kpc, units.Unit_Ms, units.Unit_Gyr, units.Unit_K])

        val = nb.CosmicTime(units=out_units.UnitTime)

        return val, label

    elif val == 'aFe':
        label = r'$\left[ \rm{Fe}/\rm{H} \right]$'
        val = nb.metals[:, 0]
        return val, label

    elif val == 'aMgFe':
        label = r'$\left[ \rm{Fe}/\rm{H} \right]$'
        val = nb.metals[:, 1] / (nb.metals[:, 0] + 1e-20)
        return val, label

    elif val == 'Fe':
        label = r'$\left[ \rm{Fe}/\rm{H} \right]$'
        val = nb.Fe()
        return val, label

    elif val == 'Metals':
        label = r'$Z$'
        val = nb.Z(mode="fraction")
        return val, label

    elif val == 'Mg':
        label = r'$\left[ \rm{Mg}/\rm{H} \right]$'
        val = nb.Mg()
        return val, label

    elif val == 'MgFe':
        label = r'$\left[ \rm{Mg}/\rm{Fe} \right]$'
        val = nb.MgFe()
        return val, label

    # abundance ratio : X/Y
    elif val.find("/") != -1:

        elt1, elt2 = val.split('/')
        label = r'$\left[ \rm{%s}/\rm{%s} \right]$' % (elt1, elt2)
        val = nb.AbRatio(elt1, elt2)

        return val, label

    else:
        label = r'%s' % val
        print(("val = %s" % val))
        exec("val = %s" % val)
        return val, label


#######################################
# MakePlot
#######################################


def MakePlot(files, opt):

    # some inits
    colors = pt.Colors(n=len(files))
    datas = []

    # read files
    for file in files:

        nb = pt.LoadPNbodyFile(file, opt)

        ################
        # get values
        ################

        if not opt.nopoints:
            x, xlabel = get_value_and_label(nb, opt.x)
            y, ylabel = get_value_and_label(nb, opt.y)
            
            
            if opt.z is not None:
                z, zlabel = get_value_and_label(nb, opt.z)
                data = pt.DataPoints(x, y, color=z, label=file, tpe='points')
            else:
                data = pt.DataPoints(
                    x, y, color=colors.get(), label=file, tpe='points')
            datas.append(data)

            if opt.PJeans:

                # args={"JeansHsml":opt.JeansHsml,"Softening":opt.Softening,"SofteningMaxPhys":opt.SofteningMaxPhys}
                # yj,Plabel = get_value_and_label(nb,"logTJeans",args=args)
                # data = pt.DataPoints(x,yj,color=colors.get(),label=file,tpe='points')
                # datas.append(data)

                # some constants
                gamma = nb.unitsparameters.get('gamma')
                xi = nb.unitsparameters.get('xi')
                ionisation = nb.unitsparameters.get('ionisation')
                mu = thermodyn.MeanWeight(xi, ionisation)
                mh = ctes.PROTONMASS.into(nb.localsystem_of_units)
                k = ctes.BOLTZMANN.into(nb.localsystem_of_units)
                G = ctes.GRAVITY.into(nb.localsystem_of_units)

                NJ = opt.NJ  # Jeans Mass factor

                T = nb.T()

                if opt.JeansHsml is None and opt.Softening is None and opt.SofteningMaxPhys is None:
                    Hsml = nb.SphRadius()
                else:
                    if opt.Softening is not None and opt.SofteningMaxPhys is not None:
                        print()
                        print((
                            "     using Softening = %g and  SofteningMaxPhys = %g" %
                            (opt.Softening, opt.SofteningMaxPhys)))
                        Hsml = nb.ComputeSofteningCosmo(
                            opt.Softening, opt.SofteningMaxPhys)
                        print(Hsml)
                        print()
                    else:
                        Hsml = opt.JeansHsml

                Unit_atom = ctes.PROTONMASS.into(units.cgs) * units.Unit_g
                Unit_atom.set_symbol('atom')

                out_units = units.UnitSystem(
                    'local', [units.Unit_cm, Unit_atom, units.Unit_s, units.Unit_K])
                rho_plot = nb.Rho(units=out_units.UnitDensity)

                rho = nb.Rho()

                #####################################
                # compute the crittical temperature
                #####################################

                TJ = (mu * mh) / k * 4. / np.pi * G / gamma * \
                    NJ**(2. / 3.) * Hsml**(2) * rho

                x = np.log10(rho_plot)
                y = np.log10(TJ)
                data = pt.DataPoints(x, y, color='y', label=file, tpe='points')
                datas.append(data)

                #####################################
                # compute the crittical density
                #####################################

                rhocJ = np.pi / 4.0 * k / (mu * mh) * gamma / \
                    G * NJ**(-2. / 3.) * Hsml**(-2) * T
                rhocJ = rhocJ * \
                    nb.localsystem_of_units.convertionFactorTo(out_units.UnitDensity)

                x = np.log10(rhocJ)
                y = np.log10(T)
                #data = pt.DataPoints(x,y,color='r',label=file,tpe='points')
                # datas.append(data)

                #####################################
                # unstable regions
                #####################################

                c = rho_plot > rhocJ
                x = np.log10(np.compress(c, rho_plot))
                y = np.log10(np.compress(c, T))
                data = pt.DataPoints(x, y, color='b', label=file, tpe='points')
                datas.append(data)

    norm = None
    # now, plot
    if not opt.map:
        for d in datas:

            if d.tpe == 'points' or d.tpe == 'both':

                if opt.log is not None:
                    if str.find(opt.log, 'z') != -1:
                        #norm = pt.colors.LogNorm(opt.zmin,opt.zmax)
                        norm = pt.colors.LogNorm()
                else:
                    norm = None

                cmap = pt.GetColormap('rainbow4', revesed=True)
                # pt.scatter(d.x,d.y,c=d.color,s=5,linewidths=0,marker='o',vmin=opt.zmin,vmax=opt.zmax,norm=norm,cmap=cmap)
                pt.scatter(
                    d.x,
                    d.y,
                    c=d.color,
                    edgecolor=d.color,
                    s=1,
                    linewidths=1,
                    marker='o',
                    vmin=opt.zmin,
                    vmax=opt.zmax,
                    norm=norm,
                    cmap=cmap)

            if d.tpe == 'line' or d.tpe == 'both':
                pt.plot(d.x, d.y, color=d.color)

    # set limits and draw axis
    xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(
        opt.xmin, opt.xmax, opt.ymin, opt.ymax, datas, opt.log)

    if opt.map:
        # now, plot
        for d in datas:
            x = d.x
            y = d.y
            z = np.zeros(len(x))

            # set log
            # if log!=None:
            #  if string.find(opt.log,'x') != -1:
            #    x = np.log10(x)
            #  if string.find(opt.log,'y') != -1:
            #    y = np.log10(y)

            c = np.isreal(x) * np.isreal(y)
            x = np.compress(c, x)
            y = np.compress(c, y)

            # re-scale between 0 and 1
            xs = 1 - (x - xmax) / (xmin - xmax)
            ys = 1 - (y - ymax) / (ymin - ymax)

            pos = np.transpose(np.array([xs, ys, z], np.float32))
            m = np.ones(len(x), np.float32)

            data = mapping.mkmap2d(pos, m, m, (opt.nx, opt.ny))

            if np.sum(np.ravel(data)) > 0:
                data = np.transpose(data) / np.sum(np.ravel(data))

            #########################
            # compute and draw level
            #########################

            rdata = np.ravel(data)
            rdata = rdata / np.sum(rdata)

            zmin = np.min(rdata)
            zmax = np.max(rdata)

            def levfct(x, flev):
                da = np.where(rdata < x, 0., rdata)
                return np.sum(da) - flev

            if np.sum(np.ravel(data)) > 0:

                # here, we cut at 90% of the flux
                levelmax = zmax
                levelmin = optimize.bisect(
                    levfct, a=zmin, b=zmax, args=(
                        0.90,), xtol=1e-10, maxiter=100)

                cmap = pt.GetColormap('heat', revesed=True)
                #im = pt.imshow(data, interpolation='bilinear', origin='lower',cmap=cmap, extent=(xmin,xmax,ymin,ymax),aspect='auto')
                im = pt.imshow(
                    data,
                    interpolation='bilinear',
                    origin='lower',
                    cmap=cmap,
                    extent=(
                        xmin,
                        xmax,
                        ymin,
                        ymax),
                    aspect='auto',
                    vmin=levelmin,
                    vmax=levelmax)

            #cset = pt.contour(data,np.array([levelmin]),origin='lower',linewidths=1,extent=(xmin,xmax,ymin,ymax))

            #########################
            # compute monte carlo
            #########################

            if opt.mc > 0:
                from pNbody import montecarlolib

                datamc = np.transpose(data)

                pos = montecarlolib.mc2d(
                    datamc.astype(np.float32), opt.mc, np.random.random() * 1000)
                # transform into physical coord
                x = pos[:, 0]
                y = pos[:, 1]
                x = x * (xmax - xmin) + xmin
                y = y * (ymax - ymin) + ymin
                # pt.scatter(x,y,marker='o',s=1)
                xe = 0.1 * np.ones(len(y))
                ye = 0.2 * np.ones(len(y))
                pt.errorbar(
                    x,
                    y,
                    xerr=xe,
                    yerr=ye,
                    fmt=None,
                    ecolor='g',
                    lw=0.1)

    # plot axis
    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)
    pt.xlabel(xlabel, fontsize=pt.labelfont)
    pt.ylabel(ylabel, fontsize=pt.labelfont)
    pt.grid(False)

    if opt.legend:
        pt.LegendFromDataPoints(datas)

    if opt.colorbar:

        #lev = np.array([-2,0,15])
        #l_f = pt.LogFormatter(10, labelOnlyBase=False)
        #cb = pt.colorbar(ticks=lev, format = l_f)

        cb = pt.colorbar()
        cb.set_label(zlabel)

########################################################################
# MAIN
########################################################################


if __name__ == '__main__':
    files, opt = parse_options()
    pt.InitPlot(files, opt)
    pt.pcolors
    MakePlot(files, opt)
    pt.EndPlot(files, opt)
