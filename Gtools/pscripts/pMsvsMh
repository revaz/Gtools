#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      pMsvsMh
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################


from optparse import OptionParser
import Ptools as pt
from pNbody import *


def parse_options():
    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_postscript_options(parser)

    parser.add_option("--legend",
                      action="store_true",
                      dest="legend",
                      default=False,
                      help="add a legend")
    (options, args) = parser.parse_args()

    files = args

    return files, options


def ReadInfoFile(file):
    import pickle

    f = open(file, 'r')
    GlobalDict = pickle.load(f)
    f.close()

    return GlobalDict


def get_value_and_label(dic, var):
    if var == 'M200':
        label = r'$\rm{M_{200}}\,\left[ M_{\odot} \right]$'

        val = []
        for model in dic.keys():
            val.append(dic[model][var])

        val = np.array(val)

        return val, label

    if var == 'MRt':
        label = r'$\rm{M_{Rt}}\,\left[ M_{\odot} \right]$'

        val = []
        for model in dic.keys():
            val.append(dic[model][var])

        val = np.array(val)

        return val, label

    if var == 'Ms':
        label = r'$\rm{M_{\star}}\,\left[ M_{\odot} \right]$'

        val = []
        for model in dic.keys():
            val.append(dic[model][var])

        val = np.array(val)

        return val, label


#######################################
# MakePlot
#######################################


def MakePlot(files, opt):
    file = files[0]

    # 3
    # simulations

    # Pelupessy
    Mstar_Pelupessy = [18e7]
    Mh_Pelupessy = [15e9]
    label = r"$\rm{Pelupessy\,et\,al.\,2004}$"
    pt.scatter(Mh_Pelupessy, Mstar_Pelupessy, c='c', label=label)

    # Stinson dwarfs 2009
    Mstar_Stinson = [7.86e7, 22e7, 38.6e7, 1.72e7]
    Mtot_Stinson = [5e9, 8.6e9, 14e9, 14e9]
    label = r"$\rm{Stinson\,et\,al.\,2007,\,2009}$"
    pt.scatter(Mtot_Stinson, Mstar_Stinson, c='g', label=label)

    # valcke
    Mstar_Valke = [57.9e7, 48.8e7]
    Mh_Valke = [4.1e9, 4.1e9]
    label = r"$\rm{Valcke\,et\,al.\,2008}$"
    pt.scatter(Mh_Valke, Mstar_Valke, c='b', label=label)

    # Governato
    Mstar_Governato = [48e7, 18e7]
    Mh_Governato = [35e9, 20e9]
    label = r"$\rm{Governato\,et\,al.\,2010}$"
    pt.scatter(Mh_Governato, Mstar_Governato, c='y', label=label)

    # Maschenko
    Mstar_Maschenko = [1e7]
    Mh_Maschenko = [2e9]
    label = r"$\rm{Maschenko\,et\,al.\,2008}$"
    pt.scatter(Mh_Maschenko, Mstar_Maschenko, c='m', label=label)

    # Sawala
    Mstar_Till = np.array([7.81, 5.25, 4.94, 8.14, 10.2, 6.16]) * 1e7
    Mh_Till = np.array([9.41, 8.34, 8.8, 8.98, 9.94, 8.54]) * 1e9
    label = r"$\rm{Sawala\,et\,al.\,2011}$"
    pt.scatter(Mh_Till, Mstar_Till, c='r', label=label)

    # Revaz
    Mstar_Revaz = np.array([1.35, 0.34, 0.07, 0.02]) * 1e7
    Mh_Revaz = np.array([7, 5, 3, 1]) * 1e8
    label = r"$\rm{Revaz\,\,et\,\,al.\,\,2012}$"
    pt.scatter(Mh_Revaz, Mstar_Revaz, c='k', label=label)

    # GDAd = ReadInfoFile("sims072-091.dmp")
    #
    # x = GDAd["M_halo"] * 1e8
    # y = GDAd["M_stars"]* 1e8
    #
    # label=r"$\rm{Revaz\,\,et\,\,al.\,\,2012}$"
    # pt.scatter(x, y,c='black',label=label)

    # LCDM2

    import pickle

    f = open(file, 'r')
    dic = pickle.load(f)
    f.close()

    cMstar, xl = get_value_and_label(dic, 'Ms')
    cM200, xl = get_value_and_label(dic, 'M200')

    label = r"$\rm{Zoom-in\,\,\Lambda CDM}$"
    pt.scatter(cM200, cMstar, c='cyan', label=label, s=50, marker='p')

    # behroozi 2013

    Mh = np.arange(5, 15, 0.1)
    Mh = 10 ** Mh

    eps = 10 ** (-1.777)
    M1 = 10 ** (11.514)
    a = -1.412
    d = 3.508
    g = 0.316

    def f(x):
        return -np.log10(10 ** (a * x) + 1) + d * \
               (((np.log10(1 + np.exp(x))) ** g) / (1. + np.exp(10 ** (-x))))

    logMs = np.log10(eps * M1) + f(np.log10(Mh / M1)) - f(0)

    Ms = 10 ** logMs

    pt.plot(Mh, Ms, lw=10, alpha=0.5)

    xmin = 1e7
    xmax = 10 ** 12
    ymin = 10 ** 3
    ymax = 10 ** 11

    # 3
    # sdss

    alpha = 0.926
    beta = 0.261
    gamma = 2.440
    c = 0.129
    M0 = 10 ** (11.4)

    logMh = np.arange(np.log10(xmin), np.log10(xmax), 0.1)
    Mh = 10 ** logMh
    Mstars = Mh * c * ((Mh / M0) ** (-alpha) + (Mh / M0) ** (beta)) ** (-gamma)

    # Guo
    pt.plot(Mh, Mstars, 'k')

    # Till extrapolation
    x = np.array([2e9, 1e10, 6e10])
    y = np.array([5e5, 1e7, 2e8])
    # pt.plot(x,y,'k')

    alpha = 0.926
    beta = 0.261
    gamma = 0.85
    c = 0.012
    M0 = 10 ** (11.4)

    Mh = np.compress(Mh < 6.31e10, Mh)

    Mstars = Mh * c * ((Mh / M0) ** (-alpha) + (Mh / M0) ** (beta)) ** (-gamma)

    pt.plot(Mh, Mstars, 'k')

    ######################################################################

    pt.SetAxis(xmin, xmax, ymin, ymax, log='xy')

    pt.xlabel(r'$\rm{M_{\rm{halo}}}\,\rm{[M_\odot]}$', fontsize=pt.labelfont)
    pt.ylabel(r'$\rm{M_\star}\,\rm{[[M_\odot]}$', fontsize=pt.labelfont)

    if opt.legend:
        pt.legend(loc='upper left')


########################################################################
# MAIN
########################################################################


if __name__ == '__main__':
    files, opt = parse_options()
    opt.size = (8, 8)
    pt.InitPlot(files, opt)
    pt.pcolors
    MakePlot(files, opt)
    pt.EndPlot(files, opt)
