#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      pOuterPotential
#  brief:     Plot properties of an outer potential
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################

"""
Plot properties of an outer potential
"""

import string
import os
import numpy as np
import Ptools as pt

from optparse import OptionParser
from Gtools import orbitalIntegration
from astropy.cosmology import Planck15, z_at_value
from astropy import units


class Units:
    """
    Mimic parser properties
    """

    def __init__(self, UL, UM, UT):
        self.UL = UL
        self.UM = UM
        self.UT = UT


def redshift(t):
    # t in Gyr
    z = z_at_value(Planck15.age, t * units.Gyr)
    return z


def scaleFactor(z):
    return 1. / (1. + z)


def zFromScaleFactor(a):
    return 1. / a - 1.


def getRedshiftTicks(zmin, zmax, N):
    amin = scaleFactor(zmin)
    amax = scaleFactor(zmax)
    ticks = np.linspace(amax, amin, N)
    ticks = zFromScaleFactor(ticks).round(decimals=2)
    labels = ['{:g}'.format(z) for z in ticks]
    ticks = Planck15.age(ticks).to("Gyr").value
    return ticks, labels


def parse_options():
    """
    Parse options
    """
    usage = "Plot properties of an outer potential"
    usage += "usage: %prog [options] directories\n"
    usage += "Example: pOuterPotential --legend --legend_loc='lower right' --skip_concentration LouiseDir"
    parser = OptionParser(usage=usage)

    options = [
        "postscript",
        "limits",
        "log",
        "legend"
    ]

    defaults = {}
    defaults["limits"] = {
        'xmin': 0,
        'xmax': 14,
        'ymin': 0.,
    }

    parser.add_option("-N", "--nber",
                      action="store",
                      dest="nber",
                      default=1000,
                      help="Number of data")

    parser.add_option("-t", "--time",
                      action="store",
                      dest="time",
                      default=13.799,
                      help="Time used to evaluate the potential (Gyr)")

    parser.add_option("--ne_ref",
                      action="store",
                      dest="ne_ref",
                      default=1e-4,
                      help="Reference value at 50kpc for the electron density")

    parser.add_option("--skip_concentration",
                      action="store_true",
                      dest="skip_c",
                      default=False,
                      help="Skip the concentration in the time evolution plot")

    parser.add_option("--redshift_min",
                      action="store",
                      dest="redshift_min",
                      default=0,
                      type=float,
                      help="Minimum redshift to show",)

    parser.add_option("--redshift_max",
                      action="store",
                      dest="redshift_max",
                      default=6,
                      type=float,
                      help="Maximum redshift to show",)

    parser.add_option("--N_redshift",
                      action="store",
                      dest="N_redshift",
                      default=10,
                      type=int,
                      help="Number of redshift to show",)

    directories, options = pt.parse_options(parser=parser, options=options,
                                            default_params=defaults)

    return directories, options


#######################################
# MakePlot
#######################################


def MakePlot(directories, opt):

    pt.InitPlot(directories, opt)
    pt.pcolors

    # some inits
    colors = pt.Colors(n=len(directories))
    colors2 = pt.Colors(n=6)
    datas = []
    density = []
    time_ev = []

    # read files
    # loop over all potentials
    for directory in directories:

        ################
        # get values
        ################
        x = np.linspace(opt.xmin, opt.xmax, opt.nber)
        y = np.zeros(opt.nber)
        files = []
        T = None
        potentials = []
        # loop over files in a potential
        for (dirpath, dirnames, filenames) in os.walk(directory):
            files.extend(filenames)
        # loop over all files in a potential directory
        # and compute the time evolution of theses files
        for f in files:
            pot = orbitalIntegration.Potential(directory + "/" + f)
            potentials.append(pot)
            y += pot.pot(opt.time, x * orbitalIntegration.kpc)
            # check if time dependend potential
            if "Evolving" in pot.pot_type:
                # get list of fields
                if "NFW" in pot.pot_type:
                    var = ["Mvir", "rvir", "rs"]
                elif "Plummer" in pot.pot_type:
                    var = ["Mvir", "soft"]
                elif "Temperature" in pot.pot_type:
                    T = pot.T(opt.time)
                    continue
                else:
                    print("Profile %s not implemented" % pot.pot_type)
                    exit()

                # loop over all fields
                for i in var:
                    norm = getattr(pot, i)(pot.t)[-1]
                    if i == "Mvir":
                        text = "$M_{vir}$"
                    elif i == "rs":
                        text = "$c$"
                    elif i == "rvir":
                        text = "$R_{vir}$"
                    else:
                        text = i
                    value = getattr(pot, i)(pot.t) / norm
                    if i == "rs":
                        value = pot.rvir(pot.t) / (value * pot.rvir(pot.t[-1]))
                    # save data
                    data = pt.DataPoints(pot.t, value, color=colors2.get(),
                                         label=text, tpe="both")
                    if i == "rs" and opt.skip_c:
                        continue
                    time_ev.append(data)

        c = colors.get()
        # compute the density
        if T is None:
            print("Setting temperature to default")
            T = 2e6

        units = Units(1., 1.6726e-24, 1.)  # transform into atom / cm3
        rho = np.zeros(x.shape)
        for i in range(len(x)):
            x_tmp = x[i] * orbitalIntegration.kpc
            tmp, _, _ = orbitalIntegration.getHaloDensity(
                x_tmp, opt.time, opt.ne_ref, T, potentials,
                h_frac=0.75, opt=units, ionized=False, today=13.8)

            rho[i] = tmp

        tmp_density = pt.DataPoints(x, np.log10(rho), color=c, label=directory,
                                    tpe="points")
        density.append(tmp_density)

        # potential
        data = pt.DataPoints(x, y, color=c, label=directory,
                             tpe='points')
        datas.append(data)

    # plot potential
    norm = None
    for d in datas:
        if opt.log is not None:
            if string.find(opt.log, 'z') != -1:
                norm = pt.colors.LogNorm()
            else:
                norm = None

        cmap = pt.GetColormap('rainbow4', revesed=True)
        pt.scatter(d.x, d.y, c=d.color, edgecolor=d.color, s=1, linewidths=1,
                   marker='o', vmin=opt.zmin, vmax=opt.zmax, norm=norm,
                   cmap=cmap)

    # set limits and draw axis
    xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt.xmin, opt.xmax,
                                                        opt.ymin, opt.ymax,
                                                        datas, opt.log)

    # plot axis
    xlabel = r"$\rm{Distance [kpc]}$"
    ylabel = r"$\rm{Potential}\; [\rm{kpc}^2/\rm{Gyr}^2]$"
    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)
    pt.xlabel(xlabel, fontsize=pt.labelfont)
    pt.ylabel(ylabel, fontsize=pt.labelfont)
    pt.grid(False)

    if opt.legend:
        pt.LegendFromDataPoints(datas, loc=opt.legend_loc)

    # plot density
    pt.InitPlot(directories, opt)

    norm = None
    for d in density:
        if opt.log is not None:
            if string.find(opt.log, 'z') != -1:
                norm = pt.colors.LogNorm()
            else:
                norm = None

        cmap = pt.GetColormap('rainbow4', revesed=True)
        pt.scatter(d.x, d.y, c=d.color, edgecolor=d.color, s=1, linewidths=1,
                   marker='o', vmin=opt.zmin, vmax=opt.zmax, norm=norm,
                   cmap=cmap)

    # set limits and draw axis
    ymin, ymax = pt.gca().get_ylim()
    xmin, xmax, ymin, ymax = pt.SetLimitsFromDataPoints(opt.xmin, opt.xmax,
                                                        ymin, ymax,
                                                        datas, opt.log)

    # plot axis
    xlabel = r"$\rm{Distance [kpc]}$"
    ylabel = r"$\rm{Log Density [atom/cm3]}$"
    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)
    pt.xlabel(xlabel, fontsize=pt.labelfont)
    pt.ylabel(ylabel, fontsize=pt.labelfont)
    pt.grid(False)

    if opt.legend:
        pt.LegendFromDataPoints(datas, loc=opt.legend_loc)

    # plot scaling
    pt.InitPlot(directories, opt)

    norm = None
    for d in time_ev:
        pt.plot(d.x, d.y, color=d.color, label=d.label)

    # set limits and draw axis
    # xmin,xmax,ymin,ymax = pt.SetLimitsFromDataPoints(7,7,0,1,time_ev,opt.log)
    for i in time_ev:
        ymin = min(ymin, i.y.min())
        ymax = max(ymax, i.y.max())
    xlabel = r"$\rm{Time}\; [\rm{Gyr}]$"
    if opt.skip_c:
        ylabel = r"$\rm{Normalized}\; R_{vir},\; M_{vir}$"
    else:
        ylabel = r"$\rm{Normalized}\; R_{vir},\; M_{vir},\; c$"
    pt.xlabel(xlabel, fontsize=pt.labelfont)
    pt.ylabel(ylabel, fontsize=pt.labelfont)

    if opt.legend:
        pt.LegendFromDataPoints(time_ev, loc=opt.legend_loc, protect=False)

    pt.grid(False)
    pt.plot([xmin, xmax], [1, 1], "--k")
    ymin = 0.
    pt.xlim([xmin, xmax])
    pt.ylim([ymin, ymax])

    # add redshift
    ax1 = pt.gca()
    ax2 = ax1.twiny()
    ticks, labels = getRedshiftTicks(opt.redshift_min,
                                     opt.redshift_max,
                                     opt.N_redshift)
    ax2.set_xticks(ticks)
    ax2.set_xticklabels(labels)
    xlabel = r"$\rm{Redshift}$"
    ax2.set_xlabel(xlabel, fontsize=pt.labelfont)

    pt.EndPlot(directories, opt)
########################################################################
# MAIN
########################################################################


if __name__ == '__main__':
    directories, opt = parse_options()
    MakePlot(directories, opt)
