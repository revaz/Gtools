#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      Gtools/cosmo.py
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################
import numpy as np
from pNbody.ctes import *
from pNbody import units
import cgtools

##########################################################################
#
# THERMODYNAMIC RELATIONS
#
##########################################################################

# deflauts cosmo parameters
defaultpars = {"Hubble": 0.1, "OmegaLambda": 0.76, "Omega0": 0.24}


###################
def Z_a(a):
    ###################
    """
    z(a)
    """
    return 1. / a - 1

###################


def A_z(z):
    ###################
    """
    a(z)
    """
    return 1 / (z + 1)

###################


def Hubble_a(a, pars=defaultpars):
    ###################
    """
    H(a)
    """
    OmegaLambda = pars['OmegaLambda']
    Omega0 = pars['Omega0']
    Hubble = pars['Hubble']

    hubble_a = Omega0 / (a * a * a) + (1 - Omega0 -
                                       OmegaLambda) / (a * a) + OmegaLambda
    hubble_a = Hubble * np.sqrt(hubble_a)

    return hubble_a


###################
def Adot_a(a, pars=defaultpars):
    ###################
    """
    da/dt
    """
    OmegaLambda = pars['OmegaLambda']
    Omega0 = pars['Omega0']
    Hubble = pars['Hubble']

    hubble_a = Omega0 / (a * a * a) + (1 - Omega0 -
                                       OmegaLambda) / (a * a) + OmegaLambda
    hubble_a = Hubble * np.sqrt(hubble_a)
    adot_a = hubble_a * a

    return adot_a

###################


def Age_a(a, pars=defaultpars):
    ###################
    """
    cosmic age as a function of a
    """
    OmegaLambda = pars['OmegaLambda']
    Omega0 = pars['Omega0']
    Hubble = pars['Hubble']

    a = np.array([a], float)
    LITTLEH = 0.7
    age_a = cgtools.Age_a(a, Omega0, OmegaLambda, Hubble) * 0.978 / LITTLEH

    return age_a
