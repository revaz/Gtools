#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      Gtools/orbitalIntegration.py
#  brief:     Compute the orbit inside a gravitational potential
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Matthew Nichols, Loic Hausammann <loic_hausammann@hotmail.com>
#
# This file is part of Gtools.
###########################################################################################
"""
Compute the orbit inside a gravitational potential
"""

import numpy as np
from scipy import integrate, interpolate
from scipy.optimize import newton
from copy import deepcopy
from pNbody import units
from pNbody import ctes
from pNbody import thermodyn


G = ctes.GRAVITY.into(units.cgs)
kpc = units.Unit_kpc.bases[0].factor * 100
Msol = units.Unit_Msol.bases[0].factor * 1000
k_b = ctes.BOLTZMANN.into(units.cgs)
amu = ctes.PROTONMASS.into(units.cgs)

#################################


def Cylindrical2Cartesian(orbit, opt=None):
    #################################
    """
    Transform the units from Cylindrical coordinates to Cartesian
    If opt is given, do a change of units with opt.UV and opt.UL
    :params np.array(:,4) orbit: Position to transform (r,theta, vr, dtheta)
    :params opt: Structure containing the units
    :returns: np.array(:,4) in cartesian units (x,y,vx,vy)
    """
    dim1 = False
    if len(orbit.shape) == 1:
        dim1 = True
    orbit = np.atleast_2d(orbit)
    temp = deepcopy(orbit)
    temp[:, 3] *= temp[:, 0]  # v_\theta = r*d\theta
    if opt is not None:
        temp[:, 0] /= opt.UL
    temp[:, 1] = temp[:, 0] * np.sin(orbit[:, 1])  # y = r*np.sin(\theta)
    temp[:, 0] = temp[:, 0] * np.cos(orbit[:, 1])  # x = r*np.cos(\theta)
    if opt is not None:
        temp[:, 2] /= opt.UV  # v_r
        temp[:, 3] /= opt.UV  # v_\theta
    v_r = deepcopy(temp[:, 2])
    v_theta = deepcopy(temp[:, 3])
    temp[:, 2] = v_r * np.cos(orbit[:, 1]) - v_theta * \
        np.sin(orbit[:, 1])  # v_x
    temp[:, 3] = v_r * np.sin(orbit[:, 1]) + v_theta * \
        np.cos(orbit[:, 1])  # v_y
    if dim1:
        temp = temp[0, :]
    return temp


#################################
class Potential:
    #################################
    """
    Defines a Potential from a file.
    Possible formats are given in the Potential directory with the
    units in parenthesis (if the time is given, multiples lines
    are required).
    """
    #################################

    def __init__(self, path):
        #################################
        """
        Create a potential object
        :param string path: Path of the file
        """
        self.path = path
        f = open(path)
        self.pot_type = f.readline().strip()
        f.close()
        if self.pot_type == "Evolving_NFW":
            tmp = np.loadtxt(path, skiprows=1, unpack=True)
            t = tmp[0]
            self.t = t

            Mvir = tmp[1]
            self.Mvir = interpolate.interp1d(t, Mvir, kind='linear')

            rvir = tmp[2]
            self.rvir = interpolate.interp1d(t, rvir, kind='linear')

            rs = tmp[3]
            self.rs = interpolate.interp1d(t, rs, kind='linear')

            if len(tmp) > 4:
                temp = tmp[4]
                self.temp = interpolate.interp1d(t, temp, kind='linear')

        elif self.pot_type == "Evolving_Plummer":
            t, Mvir, soft = np.loadtxt(path, skiprows=1, unpack=True)
            self.t = t
            self.Mvir = interpolate.interp1d(t, Mvir, kind='linear')
            self.soft = interpolate.interp1d(t, soft, kind='linear')
        elif self.pot_type == "NFW":
            # mimic the other potential
            Mvir, rvir, rs = np.loadtxt(path, skiprows=1, unpack=True)
            self.Mvir = lambda x: Mvir
            self.rvir = lambda x: rvir
            self.rs = lambda x: rs
        elif self.pot_type == "Plummer":
            Mvir, soft = np.loadtxt(path, skiprows=1, unpack=True)
            self.Mvir = lambda x: Mvir
            self.soft = lambda x: soft
        elif self.pot_type == "Evolving_Temperature":
            t, T = np.loadtxt(path, skiprows=1, unpack=True)
            self.t = t
            self.T = interpolate.interp1d(t, T, kind='linear')
        else:
            print(("Potential '%s' not recognized in file %s" % (self.pot_type,
                                                                 self.path)))
            exit(12)

    #################################
    def write(self, filename):
        #################################
        """
        Write potential to a file
        :param str filename: Output filename
        """
        print((
            "Writing Potential File %s with a potential of type %s" %
            (filename, self.pot_type)))
        with open(filename, "w") as f:
            f.write(self.pot_type + "\n")
            if self.pot_type == "NFW":
                f.write(str(self.Mvir(0)) + "\t" +
                        str(self.rvir(0)) + "\t" + str(self.rs(0)) + "\n")
            if self.pot_type == "Plummer":
                f.write(str(self.Mvir(0)) + "\t" + str(self.soft(0)) + "\n")
            else:
                for i in range(len(self.t)):
                    txt = str(self.t[i]) + "\t"
                    txt = txt + str(self.Mvir(self.t[i])) + "\t"
                    if self.pot_type == "Evolving_NFW":
                        txt = txt + str(self.rvir(self.t[i])) + "\t"
                        txt = txt + str(self.rs(self.t[i]))
                    elif self.pot_type == "Evolving_Plummer":
                        txt = txt + str(self.soft(self.t[i]))
                    else:
                        print(("Potential %s not recognized" % self.pot_type))
                    f.write(txt + "\n")
        f.close()

    #################################
    def pot(self, t, r):
        #################################
        """
        Compute the potential at the given time and radius.
        Accept numpy array for the position.
        :param float t: Time (in Gyr)
        :param float r: Radius (in cgs)
        :returns: Potential in cgs
        """
        if self.pot_type == 'NFW' or self.pot_type == "Evolving_NFW":
            mvir = self.Mvir(t) * Msol
            rvir = self.rvir(t) * kpc
            rs = self.rs(t) * kpc
            cvir = rvir / rs
            return -G * mvir / (np.log(1 + cvir) - cvir /
                                (1 + cvir)) * np.log(1 + r / rs) / r
        elif self.pot_type == 'Plummer' or self.pot_type == "Evolving_Plummer":
            mvir = self.Mvir(t) * Msol
            soft = self.soft(t) * kpc
            return -G * mvir / np.sqrt(r**2 + soft**2)
        elif self.pot_type == "Evolving_Temperature":
            return 0.
        else:
            print(("Potential '%s' not recognized" % self.pot_type))
            exit(143)

    #################################
    def mass(self, t, r):
        #################################
        """
        Compute the mass contained inside a sphere of radius r.
        Accept numpy array for the position.
        :param float t: Time (in Gyr)
        :param float r: Radius (in cgs)
        :returns: mass in cgs
        """
        if self.pot_type == 'NFW' or self.pot_type == "Evolving_NFW":
            mvir = self.Mvir(t) * Msol
            rvir = self.rvir(t) * kpc
            rs = self.rs(t) * kpc
            cvir = rvir / rs

            def profile(p):
                return np.log(1. + p) - p / (1. + p)
            return mvir * profile(r / rs) / profile(cvir)
        elif self.pot_type == 'Plummer' or self.pot_type == "Evolving_Plummer":
            mvir = self.Mvir(t) * Msol
            soft = self.soft(t) * kpc
            return mvir * r**3 / (r**2 + soft**2)**1.5
        elif self.pot_type == "Evolving_Temperature":
            return 0.
        else:
            print(("Potential '%s' not recognized" % self.pot_type))
            exit(143)

    #################################
    def dVdr(self, t, r):
        #################################
        """
        Compute the radial derivative of the potential at the given
        time and radius.
        Accept numpy array for the position.
        :param float t: Time (in Gyr)
        :param float r: Radius (in cgs)
        :returns: dV/dr in cgs
        """
        if self.pot_type == 'NFW' or self.pot_type == "Evolving_NFW":
            mvir = self.Mvir(t) * Msol
            rvir = self.rvir(t) * kpc
            rs = self.rs(t) * kpc
            cvir = rvir / rs
            return -G * mvir / ((np.log(1 + cvir) - cvir / (1 + cvir))) * \
                ((r + rs) * np.log((r + rs) / rs) - r) / (r**2 * (r + rs))
        elif self.pot_type == 'Plummer' or self.pot_type == "Evolving_Plummer":
            mvir = self.Mvir(t) * Msol
            soft = self.soft(t) * kpc
            return -r * G * mvir / (r**2 + (soft)**2)**1.5
        elif self.pot_type == "Evolving_Temperature":
            return 0.
        else:
            print(("Potential '%s' not recognized" % self.pot_type))
            exit(144)

    #################################
    def rotation2(self, t, r):
        #################################
        """
        Compute the squared rotation "curve" for the halo
        :param float t: Time (in Gyr)
        :param float r: Radius (in cgs)
        :returns: Velocity in cgs
        """
        return G * self.mass(t, r) / r


#################################
def orbiteq(x, t, Lsq, potentials):
    #################################
    """
    Compute the generalized forces in the lagrangian equation of orbital motion.
    .. math::
       \frac{dq}{dt} = f(q)
       f_0(q) = v_r
       f_1(q) = \dot{\theta}
       f_2(q) = \frac{L^2}{r^2} + \sum_i \phi_i(t,r)
       f_3(q) = -2\frac{v_r\dot{\theta}}{r}
    where :math:`q=(r, \theta, v_r, \dot{\theta})`
    :params list x: General position (q)
    :param float t: Current time (Gyr)
    :param float Lsq: Squared angular momentum
    :returns: Generalized forces
    """
    dL = np.zeros(4)
    # radius
    dL[0] = x[2]
    # angle
    dL[1] = x[3]
    # radial velocity
    dL[2] = Lsq / x[0]**3 + \
        sum([i.dVdr(t / units.Unit_Gyr.bases[0].factor, x[0]) for i in potentials])
    # angular velocity
    dL[3] = -2 * x[2] * x[3] / x[0]
    return dL

#################################


def computeOrbitCharacteristics(r_pos, vel, t_init, potentials, scale_init=2.):
    #################################
    """
    Compute the apogalacticon and the perigalacticon from the velocity
    :param float r_pos: Current distance from the center
    :param array vel: Velocity in x,y
    :param float t_init: Time where to stop
    :param ArgumentParser opt: Argument parser
    :param [Potential] potentials: List of Potential
    :param float scale_init: scaling of r_pos to use for the initial guess (>1)
    :returns: Apogalacticon, Perigalacticon
    """
    if len(vel) != 2:
        raise Exception("Should give velocity in 2D")
    L = r_pos * vel[1]
    E = 0.5 * np.sum(vel**2)

    def f(x):
        pot = sum([i.pot(t_init, x) for i in potentials])
        return 0.5 * L**2 / x**2 + pot - E

    ra = newton(f, scale_init * r_pos)
    rp = newton(f, r_pos / scale_init)

    if abs(ra - rp) < 1e-4 * ra:
        raise Exception("Unable to find right point")
    return ra, rp

#################################


def computeOrbit(ra, rp, r_pos, t_init, opt, potentials):
    #################################
    """
    Compute the full orbit of the object (backward integration) and
    save it in a pickle file
    if requested.
    Opt should contains: time (final time), rvsign (r*v sign of at opt.time)
    and orbit (filename for saving the orbit or None if not wanted)
    :param float ra: Apogalacticon
    :param float rp: Perigalacticon
    :param float r_pos: Current distance from the center
    :param float t_init: Time where to stop
    :param ArgumentParser opt: Argument parser
    :param [Potential] potentials: List of Potential
    :returns: Final position
    """
    # get L^2 from energy and angular momentum conservation at rp and ra
    Lsq = 2 * sum([(i.pot(t_init, ra) - i.pot(t_init, rp))
                   for i in potentials]) * (ra**2 * rp**2) / (ra**2 - rp**2)

    # Calculate the energy of the orbit, from r_pos we can then get the
    # velocities.
    E = sum([i.pot(t_init, ra) for i in potentials]) + 0.5 * Lsq / ra**2

    # Kinetic energy term
    KE = E - sum([i.pot(t_init, r_pos) for i in potentials])

    # Radial velocity
    v_R = opt.rvsign * np.sqrt(2 * KE - Lsq / r_pos**2)

    # Initial conditions
    # [r, theta, v_r, dtheta/dt]
    x0 = np.array([r_pos, 0, v_R, np.sqrt(Lsq) / r_pos**2])

    # time step for the output
    tpts = np.linspace(t_init, opt.time, 1000) * units.Unit_Gyr.bases[0].factor
    # integrate orbit (more points are computed than the one given)
    orbit = integrate.odeint(orbiteq, x0, tpts, args=(Lsq, potentials))

    # save orbit to a pickle file
    if opt.orbit is not None:
        saveOrbit(orbit, tpts, opt)

    # position of the galaxy at opt.time
    endpt = orbit[-1, :]
    return endpt


#################################
def saveOrbit(orbit, tpts, opt):
    #################################
    """
    Save the orbits with pickle.
    The file will contains an array with the timestep in the first dimension
    and the quantities t,x,y,vx,vz in the second one.
    Opt should contains: UL, UV (length/velocity in cgs), orbit (filename)
    :param np.array(N,5) orbit: Orbital position/velocity to save \
    see computeOrbit for the structure
    :param list tpts: Time associated with the timesteps
    :param ArgumentParser opt:
    """
    import pickle
    # change unit and goes from (r, theta) to (x,y)
    temp = Cylindrical2Cartesian(orbit, opt)
    temp = np.append(tpts[:, np.newaxis] /
                     units.Unit_Gyr.bases[0].factor, temp, 1)
    pickle.dump(temp, open(opt.orbit, 'wb'))


#################################
def getHaloDensity(
        R,
        time,
        ne,
        Tgas,
        potentials,
        h_frac=None,
        opt=None,
        today=None,
        old_implementation=False,
        ionized=None):
    #################################
    """
    Compute the halo density (mass) in cgs.
    Assumes a gas composed only by hydrogen and helium.
    If today is not given, compute the electron density at 50kpc for the time 'time'.
    See wiki for more informations.
    :params float R: Radius where to compute the density (in cgs)
    :params float time: Time where to compute the density (in Gyr)
    :params float ne: Electron (number) density at 50kpc (in cgs)
    :params float Tgas: Gas Temperature
    :params Potential potentials: List of potential
    :params float h_frac: Hydrogen (number) fraction
    :params opt: Units (opt.UL, opt.UM)
    :params float today: Today time (in Gyr)
    :returns: Density, Scaling, Ref. Potential
    """
    if today is None:
        today = time

    ref_pot = sum([i.pot(today, 50 * kpc) for i in potentials])

    if old_implementation:
        denscale = ne * np.exp(1. / (k_b * Tgas / (16. / 26 * amu)) * (np.sum([i.pot(
            today, 50 * kpc) - i.pot(today, 1000 * np.sqrt(3) * kpc) for i in potentials])))

        dens = 1.33 * denscale * amu * np.exp(-1. / (k_b * Tgas / (16. / 26 * amu)) * (
            np.sum([i.pot(time, R) - i.pot(time, 1000 * np.sqrt(3) * kpc) for i in potentials])))

    else:
        if ionized is None:
            mu = thermodyn.MeanWeightT(Tgas)
        else:
            mu = thermodyn.MeanWeight(h_frac, ionized)

        denscale = mu * amu * ne / (2 - h_frac)
        dphi = ref_pot - sum([i.pot(time, R) for i in potentials])
        dens = denscale * np.exp(dphi * amu * mu / (k_b * Tgas))

    if opt is not None and opt.UL is not None and opt.UM is not None:
        dens *= (opt.UL**3 / opt.UM)
        denscale *= (opt.UL**3 / opt.UM)
        ref_pot *= (opt.UT / opt.UL)**2

    return dens, denscale, ref_pot
