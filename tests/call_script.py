#!/usr/bin/env python3
"""
Define the Option and Script classes for the tests.
"""
from importlib import import_module
import Ptools
import matplotlib.pyplot as plt


class Option():
    """
    This class mimics the behavior of the
    OptionParser.
    """
    def __init__(self, **kargs):
        for i in kargs:
            setattr(self, i, kargs[i])


class Script():
    """
    This class call a script assuming that it
    contains a MakePlot function.
    """
    def __init__(self, script, do_plot=False):
        module = import_module(script)
        self.do_plot = do_plot
        self.function = module.MakePlot

    def call(self, files, **kargs):
        opt = Option(**kargs)
        if self.do_plot:
            Ptools.InitPlot(files, opt)
        self.function(files, opt)

        if self.do_plot:
            plt.close("all")
