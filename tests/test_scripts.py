#!/usr/bin/env python3
"""
Using call_script.py, we are simply calling most of the
functions present in ghelp in order to test them.
"""
from call_script import Script
from urllib.request import urlretrieve
from os.path import isfile

test_gscript = True
test_pscript = True


def downloadFile(url):
    filename = url.split("/")[-1]

    if not isfile(filename):
        urlretrieve(url, filename)

downloadFile("https://obswww.unige.ch/lastro/projects/Clastro/Gtools/unit_test/test_scripts/params")
downloadFile("https://obswww.unige.ch/lastro/projects/Clastro/Gtools/unit_test/test_scripts/snapshot_0001.hdf5")


if test_gscript:
    ################################################
    #                gscripts/
    ################################################

    ################################################
    # utilities

    # cosmic time as a function of a, da or Dloga
    scr = Script("Gtools.gscripts.gatime2Myr")
    scr.call([], GadgetParameterFile="params", a1=0.01, a2=0.02, a=None,
             da=None, Dloga=None)

    scr.call([], GadgetParameterFile="params", a1=0.1, a2=1., a=0.1914,
             da=7.56271e-9, Dloga=None)
    scr.call([], GadgetParameterFile="params", a1=0.1, a2=1., a=0.1914,
             da=None, Dloga=3.95127e-8)

    # # cosmic time as a function of Z

    scr = Script("Gtools.gscripts.gZtoGyr")
    scr.call([], GadgetParameterFile="params", Z=2.1)

    scr = Script("Gtools.gscripts.gZtoMyr")
    scr.call([], GadgetParameterFile="params", Z=2.1)

    # ################################################
    # # run preparation

    # # determine the memory allocation for gadget

    scr = Script("Gtools.gscripts.galloc")
    scr.call([], TotNumPart=0, TotN_gas=0, TotN_stars=0, NTask=1,
             StarFormationNStarsFromGas=4, PartAllocFactor=4.,
             StarsAllocFactor=0.25, sizeof_particle_data=72,
             sizeof_sph_particle_data=120, sizeof_st_particle_data=136)

    # # set output timesteps for cosmological simulations

    scr = Script("Gtools.gscripts.gmktimesteps")
    scr.call([], p=10, verbose=0, z=0)

    # # set the number of particles and masses for a cosmological simulation

    scr = Script("Gtools.gscripts.gSetCosmoBox")
    scr.call([], level=9, verbose=0, z=0, Omega0=0.315, OmegaLambda=0.685,
             OmegaBaryon=0.0486, HubbleParam=0.673, UnitLength_in_cm=3.08e21,
             UnitVelocity_in_cm_per_s=1e5, UnitMass_in_g=1.989e43,
             BoxSize=100)

    # # setup initial temperature for cosmological simulations

    scr = Script("Gtools.gscripts.ggetInitTemperatureforCosmoSims")
    scr.call([], a=None, Z=70, GadgetParameterFile="params")

    # # setup initial redshift for (zoom-in) cosmological simulations

    scr = Script("Gtools.gscripts.ggetZiforCosmoSims")
    scr.call([], GadgetParameterFile="params", N1=64, N2=512, L=3.442,
             n=0.9603, sigma8=0.829)

    # # return some info on the header (incomplete, actually return the atime)

    print("Skipping test with the gadget format")
    # gread_header    snap.dat

    # # return some info on the header

    scr = Script("Gtools.gscripts.gheader")
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params", N1=64,
             N2=512, L=3.442, ftype="gh5", pol_ratio=False)

    # # add gas to a cosmological box

    print("Skipping test with the gadget format")
    # addgas

    # # find and extract sub-haloes

    print("Skipping test, PySubFind not found")
    # scr = Script("Gtools.gscripts.gSubFind")
    # scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params", N1=64,
    #          N2=512, L=3.442,
    #          ftype="gh5", pol_ratio=False)

    # gSubFind --DesNumNgb 200 --Softening 0.015 --Nmin 100 --subHaloesDirectory  SubHaloDir --outputAllSubHaloes allSub.dat --outputHaloCleaned cleaned.dat snap.dat

    # ################################################
    # # file conversion

    # # gadget to gear
    # gad2gear gadget-file.dat gear-file.dat

    # # gadget to swift
    # gad2swift gadget-file.dat swift-file.dat

    # ################################################
    # # global statitics on the run

    # gcpu            : plot cpu info

    # gcpu                            cpu.txt
    # gcpu -o budget  --legend        cpu.txt
    # gcpu -o gravity --legend        cpu.txt
    # gcpu -o hydro   --legend        cpu.txt

    # genergy         : plot info relative to the energy

    # genergy --relative                      energy.txt
    # genergy -o EnergyKineticFeedback        energy.txt

    # gplot_energy    : plot info relative to the energy budget

    # gplot_energy --legend                   energy.txt

    # gsteps          : plot info relative to the steps

    #         gsteps                                   info.txt
    #         gsteps -rf                               info.txt
    #         gsteps --param params  --log y  --rf 100 info.txt


if test_pscript:
    # ################################################
    #                 pscripts/
    # ################################################

    # ################################################
    # # global output analysis

    # # star formation rate and stellar mass (sfr.txt)

    print("Skipping function dealing with csv output")

    # pSfrvsTime --param params -o SFR                                     sfr.txt
    # pSfrvsTime --param params -o MSTARS                                  sfr.txt
    # pSfrvsTime --param params -o MNEWSTARS                               sfr.txt

    # pSfrvsTime --param params -o sfr                                     sfr.txt
    # pSfrvsTime --param params -o mnewstars --integrate --derive --rf 100 sfr.txt
    # pSfrvsTime --param params -o mstars                                  sfr.txt

    # # supernovae                            (chimie.txt)
    # gSNvsTime -o NSNII,NSNIa --rf 1000                 chimie.txt
    # gSNvsTime -o EgySNThermal,EgySNKinetic --rf 1000   chimie.txt
    # gSNvsTime -o EgySNThermal,EgySNKinetic --integrate chimie.txt
    # gSNvsTime -o Nflag --rf 1000                       chimie.txt
    # gSNvsTime -o Nflag --integrate                     chimie.txt

    # # accretion                             (accretion.txt)
    # gAccretionvsTime -o MT       accretion.txt
    # gAccretionvsTime -o MT_th    accretion.txt
    # gAccretionvsTime -o DMDT     accretion.txt
    # gAccretionvsTime -o DMDT_th  accretion.txt

    # ################################################
    # # unit conversion

    # # change units, including the little h (Hubble parameter)
    # if it is different than 1
    scr = Script("Gtools.gscripts.gchangeunits")
    scr.call(["snapshot_0001.hdf5"], ftype="gh5", outputfile="tmp.hdf5",
             GadgetParameterFile1="params", GadgetParameterFile2="params")

    # # correct from scaling factor (and add the correct time in code unit)
    scr = Script("Gtools.gscripts.gcosmo2giso")
    scr.call(["snapshot_0001.hdf5"], ftype="gh5", outputfile="tmp.hdf5",
             GadgetParameterFile1="params", GadgetParameterFile2="params")

    # ################################################
    # # Jeans Informations

    scr = Script("Gtools.gscripts.gJeans")
    scr.call(["snapshot_0001.hdf5"], Density=1e3, Temperature=10,
             NJ=10)

    scr = Script("Gtools.pscripts.pJeansTemperature", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], xmin=-7, xmax=7,
             ymin=1, ymax=7, ParticleMass=4000, NJeans=10, Nsph=1)

    # ################################################
    # # snapshot analysis

    # # luminosity
    scr = Script("Gtools.pscripts.pget_Luminosity")
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             ftype="gh5", forceComovingIntegrationOn=False,
             forceComovingIntegrationOff=False, tnow=None)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             tnow=3000, ftype="gh5", forceComovingIntegrationOn=False,
             forceComovingIntegrationOff=False)

    # # virial radius
    scr = Script("Gtools.pscripts.pget_VirialRadius")
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             dX=200, ftype="gh5", Rmax=100)

    # # star formation rate
    scr = Script("Gtools.pscripts.pSFRvsTime", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="SFR", xmin=0, xmax=14, ftype="gh5", nx=500,
             ymin=0, ymax=14, log="", legend=False, plot_time=-1)

    # # mass vs time
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="MSTARS", xmin=0, xmax=14, ftype="gh5", nx=500,
             ymin=0, ymax=14, log="", legend=False, plot_time=-1)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="MSTARSz", xmin=0, xmax=10, ftype="gh5", nx=500,
             ymin=0, ymax=14, log="", legend=False, plot_time=-1)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="MSTARSa", xmin=0, xmax=1, ftype="gh5", nx=500,
             ymin=0, ymax=14, log="", legend=False, plot_time=-1)

    # # SFR + Mass
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="BOTH", xmin=0, xmax=14, ftype="gh5", nx=500,
             ymin=0, ymax=14, log="", legend=False, plot_time=-1)

    # # logRho - logT
    scr = Script("Gtools.pscripts.pplot", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="logrho", y="logT", xmin=-7, xmax=7,
             ymin=1, ymax=7)

    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="logrho", y="logT", xmin=-7, xmax=7,
             ymin=1, ymax=7, map=True)

    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="logrho", y="logT", xmin=-3, xmax=0,
             ymin=1, ymax=5, colorbar=True, log="z", z="Tcool",
             zmin=1, zmax=1e16)

    # # rho - T
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="rho", y="T", xmin=1e-3, xmax=1,
             ymin=10, ymax=1e5, log="xy")

    # # logrho - logP
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="logrho", y="logP", xmin=-7, xmax=7)

    # # R - T
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="R", y="logT")

    # # R - Fe
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="R", y="Fe")

    # # MgFe - Fe
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="stars1", x="Fe", y="MgFe", xmin=-4, xmax=0.5,
             ymin=-1, ymax=1.5)

    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="stars1", x="Fe", y="MgFe", xmin=-4, xmax=0.5,
             ymin=-1, ymax=1.5, map=True)

    # # Fe - Age
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="stars1", x="Age", y="MgFe", xmin=0, xmax=14,
             ymin=-4, ymax=0.5)

    # # Tcool
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="gas", x="logrho", y="Tcool", xmin=-5, xmax=2,
             ymin=0, ymax=1e16, log="y")

    # ################################
    # # metallicite
    # ################################

    # # MgFevsFe
    scr = Script("Gtools.pscripts.pMgFevsFe", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], select="stars1",
             xmin=-4, xmax=0.5, ymin=-1, ymax=1.5, map=True)

    # # BaFevsFe
    scr = Script("Gtools.pscripts.pBaFevsFe", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], select="stars1",
             xmin=-4, xmax=0.5, ymin=-1, ymax=1.5, map=True)

    # # [E1/E2] vs [E3/E4]
    scr = Script("Gtools.pscripts.pplot", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], select="stars1",
             xmin=-4, xmax=0.5, ymin=-1, ymax=1.5, map=True,
             x="Fe/H", y="Ba/Fe")

    # # NvsAge
    scr = Script("Gtools.pscripts.pNvsFe", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], select="stars1",
             GadgetParameterFile="params", x="Age", nx=14, xmin=0,
             xmax=14)

    # # NvsFe
    scr = Script("Gtools.pscripts.pNvsX", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], select="stars1",
             GadgetParameterFile="params", x="Fe", nx=40, xmin=-4,
             xmax=0.5, ymin=0, ymax=0.15)

    # ################################
    # # luminosity profile
    # ################################

    scr = Script("Gtools.pscripts.pLuminosityProfile", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             select="stars1", nr=64, HubbleParameter=0.673)

    # ################################
    # # profiles
    # ################################

    # # grille spherique
    # # density
    scr = Script("Gtools.pscripts.pSphericalProfile", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="density", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # masse
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="mass", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # integrated mass
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="imass", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # velocity disp. in z
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="sigmaz", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # circular velocity
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="vcirc", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy", eps=0.1)

    # # grille cylindrique
    # # density
    scr = Script("Gtools.pscripts.pCylindricalProfile", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="sdens", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # masse
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="mass", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # integrated mass
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="imass", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # velocity disp. in z
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="sigmaz", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # circular velocity
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="vcirc", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy")

    # # luminosity profile
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             obs="Lum", xmax=10, rmax=10, nr=64, center="histocenter",
             log="xy", select="stars1")

    # ############################################################
    # # velocities and velocity dispertions in cylindrical coords
    # ############################################################

    # # extract all
    scr = Script("Gtools.pscripts.gExtractCylindrical", do_plot=True)
    scr.call(["snapshot_0001.hdf5"], GadgetParameterFile="params",
             Rmax=10, components="('gas', 'stars1', 'halo1')",
             disk="('gas', stars1')", nR=32, eps=0.01, nmin=3)
    # gExtractCylindrical --Rmax 10 --components="('gas','stars1','halo1')"   --disk="('gas','stars1')" --nR 32 --eps=0.01 --nmin 3  --param params snap.dat

    print("Skipping function done on dumps")
    # # number of points per bins
    # pplotdmp --mode=Numbers                                 gas.dmp

    # # circular velocity per component
    # pplotdmp --mode=Velocities --legend                     gas.dmp stars1.dmp halo1.dmp

    # # velocities and velocity disp
    # pplotdmp --mode=Dispersions --legend                    gas.dmp

    # # stability Toomre paramter Q
    # pplotdmp --mode=Q                                       gas.dmp

    # ################################
    # # Generate and visualize palettes
    # ################################

    # # example of parameters
    # r = array([0,  255.])/255.
    # g = array([0,  254.])/255.
    # b = array([0,  208.])/255.
    # q = -1.0

    print("Skipping palette")
    # # generate a palette
    # gmkpalette --param stars.pal  -o stars

    # # visualize it
    # gplot_palette -p ./stars
