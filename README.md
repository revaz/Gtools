
Gtools python module
http://aramis.obspm.fr/~revaz//Gtools/

Release 1.0 (February, 2007)


--------------------------------------------------------------------
Requirements
--------------------------------------------------------------------
  
  * C compiler
  * Python 2.3 or higher
  * numarray (python module)
  * http://www.stsci.edu/resources/software_hardware/numarray (python module)
  * pysmongo (python module) (only needed for plotting facilities)
    http://aramis.obspm.fr/~revaz/pysmongo/
  * Nbody or pNbody (python module)
    http://aramis.obspm.fr/~revaz/pNbody/
  * POND (Poisson Solver) if using MergerTree
    http://github.com/loikki/POND
  
--------------------------------------------------------------------
Build instructions for UNIX
--------------------------------------------------------------------


1. Unpack the Gtools distribution (the file Gtools-1.0.tgz)
   in your Python extensions directory (if you have one.  If
   not, feel free to unpack it in any other suitable directory).

        $ tar -xzf Gtools-1.0.tgz


2. Using Python 2.3 or later, you can use the setup.py
   script to build the module.  The following commands should do
   the trick:

        $ python setup.py build
        $ python setup.py install

   (depending on how Python has been installed on your machine,
   you might have to log in as a superuser to run the 'install'
   command.)

   If the setup.py command finishes without any errors, you're
   done!
	   

	
--------------------------------------------------------------------
Build instructions for Windows
--------------------------------------------------------------------

1. Install Linux on your Windows machine and follow the 
   instructions for UNIX


	
--------------------------------------------------------------------	
Getting Help
--------------------------------------------------------------------

  If you have questions about python Gtools module, feel free to, write 
  to the following e-mail:

    yves.revaz@obspm.fr

