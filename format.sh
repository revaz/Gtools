#!/bin/bash

autopep8 -r -j -1 --in-place --aggressive --aggressive Gtools Doc Example old.scripts setup.py src
clang-format -style=file -i src/*/*.[ch]
