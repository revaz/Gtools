#!/bin/bash

cd tests

rm .coverage
rm .coverage.*

function test {
    $1
    CODE=$?
    if [[ $CODE -ne 0 ]]; then
	echo $1 " Exit Code " $CODE
	echo "Test failed"
	exit 1
    fi
}

test "python3 -m coverage run test_scripts.py"

python3 -m coverage combine
python3 -m coverage report
python3 -m coverage html
