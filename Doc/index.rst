.. Gtools documentation master file, created by
   sphinx-quickstart on Tue Jan 31 21:06:34 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Gtools's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   rst/Download
   rst/Examples
   rst/ChemistryStats


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

