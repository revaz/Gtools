Examples
********

Download Snapshot files
=======================


To run the following example, you need first to download some snapshot files.

 * download and extract examples ::

    wget https://obswww.unige.ch/~revaz/SnapshotsForGtools.tar.gz
    tar -xzf SnapshotsForGtools.tar.gz
    rm SnapshotsForGtools.tar.gz
    cd SnapshotsForGtools


Type ``ghelp`` to get a list of examples to run.
