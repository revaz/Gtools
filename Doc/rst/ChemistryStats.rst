ChemistryStats
**************

This is an extension module design to perform chemistry statistics on GEAR outputs.

General concepts
================

The goal of the module is to trace the succession of chemical events (supernova explosions)
that lead to the chemical properties (abundances of atomic elements) of a stellar particle.


Quick destription of ``GEAR``
"""""""""""""""""""""""""""""

Storage of chemical informations
''''''''''''''''''''''''''''''''

In ``GEAR``, a gas particle (``i``) store its chemical properties in two variables (arrays) : ``Metal`` 
(``SphP[i].Metal``) and ``MetalNS`` (``SphP[i].MetalNS``). Both variable have a dimension ``NELEMENTS`` where
``NELEMENTS`` is the number of atomic elements considered in the code. 

Every time the ejecta of a star touches a gas particle, the ejecta are added in the ``MetalNS`` in the slot of
the corresponding element.
Thus, ``MetalNS`` contains **the direct pollution of stars without any mixing process**.

On the contrary, the ``Metal`` variable is obtained by convolving the values stored in ``MetalNS``
of nearby neighbouring particles. It is a **smoothed** variable.
This convolution is performed at every time a gas particle is active (i.e, at every of its timestep).

.. math::

  \texttt{SphP[i].Metal[k]} = \frac{1}{\rho_i}\,\Sigma_j{w_{ij}\,m_j\,\texttt{SphP[j].MetalNS[k]}}

Weights
'''''''

We define the weight :math:`\alpha_{ij}` as:

.. math::

  \alpha_{ij} = \frac{w_{ij}\,m_j}{\rho_i}  
  

We define :math:`\alpha_{Z,ij}` as:

.. math::

  \alpha_{Z,ij} = \frac{Z_j\,w_{ij}\,m_j}{\rho_i}  


Note that in the code : :math:`Z_j=\texttt{SphP[j].MetalNS[METALS]}`.



Snapshots
'''''''''

Booth ``Metal`` and ``MetalNS`` may be saved in ``hdf5`` snapshots for gas particle only. For stellar particles
only the variable ``Metal`` is stored for the moment. 


Star formation
''''''''''''''

In ``GEAR``, a star particle forms out of a gas particle and inherit its chemical properties stored in 
the array ``Metal``, i.e., the abundances that have been **smoothed**.


``chimie`` log files
''''''''''''''''''''

Three types of ``chimie`` log files are generated:


``chimie.txt_Gas.*`` : These log files record every time any gas particle receive ejecta from a stellar particle.
It contains the time of the event, the ID of both the gas and the stellar particle and the mass of each ejecta.
The ejecta are provided by the variable ``MetalNS`` and thus, they are **not smoothed**.

``chimie.txt_SNs.*`` : These log files record every time a stellar particle generate one or several supernova explosion.
It provides the time, the ID of the stellar particle its mass, the number of SNIa and SNII exploded and their mean mass
[#f1]_.

``chimie.txt_Star.*`` : These log files record every time a stellar particle is formed. It records the time when the particle
is formed, the ID of the stellar particle and its mass, the ID of the progenitor gas particle and its mass, 
and a list of all gas particles that contributed
the most to the last metals mixing event, i.e., that contributed to ``SphP[i].Metal``
through the convolution defined above.
In this list, we provide the ID, the weight of the contribution both in term of mass (:math:`\alpha_{ij}`) and metals 
(:math:`\alpha_{Z,ij}`), and the mass of the particle. The list is limited to the contributors with the largest :math:`\alpha_{Z,ij}`.


The information stored in those files is thus always obtained via the ``MetalNS``. 
This ensure to get an access to un-mixed abundances, allowing to trace the **direct** pollution 
(no mixing/smoothing) of gas by stars.






``GEAR`` flags (compilation options)
''''''''''''''''''''''''''''''''''''

Both ``CHIMIE_STATS`` and ``CHIMIE_KEEP_TRACE_OF_LAST_SMOOTH_METALS`` must be enabled.



Tracing the chemical history of one stellar particle
""""""""""""""""""""""""""""""""""""""""""""""""""""

To trace the chemical history of one stellar particle, we first have to find its progenitor gas particle.
The progenitor gas particle contains chemical information that results from a smooth metallicity mixing.
The  ``GEAR`` log files stores the IDs of the gas particles that contributed to the last mixing this
particle suffers, before forming the stars.
Knowing each of these gas particles we can then follow each of them individually and retrieve each 
chemical event that contributed to the enrichment of these gas particles.
We record the information in term of mass and metallicity of the progenitor stars that exploded as a supernova,
but also the time of the explosion and the mass of each element that the gas particle received.


``ChemistryStats`` data structure
"""""""""""""""""""""""""""""""""

In the ``ChemistryStats`` module, data from the ``chimie.txt_Gas.*`` and ``chimie.txt_Star.*`` files
are stored in dictionaries ``data_Gas`` and ``data_Star``.


``data_Gas``
""""""""""""

The dictionary keys are IDs of gas particles found in the logs. Each key is linked to another dictionary
with each entry corresponding to an event, i.e., a time in the simulation when the gas particle received ejecta
from a stellar particle.

+-------------------------------------+-------------------------------------------------------------------------------------------------+
| ``data_Gas[ID]["TimeStep"]``        | Timestep of the event                                                                           |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Time"]``            | Simulation time of the event                                                                    | 
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["IDStar"]``          | ID of the stellar particle at the origin of the event                                           |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["StarMeanMass"]``    | mean of the SNe progenitor star                                                                 |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["StarFeH"]``         | [Fe/H] of the SNe progenitor star                                                               |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["aij"]``             | fraction of the ejecta of the stellar particle received by the gas particle                     |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Mass_0"]``          | mass of the particle before receiving the ejecta                                                |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Fe_0"]``            | Fe mass of the particle before receiving the ejecta                                             |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Mg_0"]``            | Mg mass of the particle before receiving the ejecta                                             |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Mass_1"]``          | mass of the particle after receiving the ejecta                                                 |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Fe_1"]``            | Fe mass of the particle after receiving the ejecta                                              |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    
| ``data_Gas[ID]["Mg_1"]``            | Mg mass of the particle after receiving the ejecta                                              |
+-------------------------------------+-------------------------------------------------------------------------------------------------+    

Note that only Fe and Mg are considered for the moment. Other elements can be easily added.


``data_Star``
"""""""""""""

The dictionary keys are IDs of all the stellar particles found. Each key is linked to another dictionary
with the following entries:

+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["TimeStep"]``       | timestep of formation time of the stellar particle                                               |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["Time"]``           | formation time of the stellar particle                                                           |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["IDprog"]``         | ID of the progenitor gas particle                                                                |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["flagToStar"]``     | flag indicating if a the gas particle as turned into a stellar particle or not                   |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["IDs"]``            | the list of ID of gas particles that contributed to the smooting of the progenitor gas particle  |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["aij"]``            | the list of corresponding weights (:math:`\alpha_{ij}`)                                          |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["Zaij"]``           | the list of corresponding Z weights (:math:`\alpha_{Z,ij}`)                                      |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
| ``data_Star[ID]["mass"]``           | the list of masses                                                                               |
+-------------------------------------+--------------------------------------------------------------------------------------------------+
 


Examples of available python scripts
====================================


Convert ``GEAR`` log files stored in ``snap`` into the ``chemistrystats.pkl`` pickle file::

  chemistryStats2pickle ./snap -o chemistrystats.pkl

Get the enrichment history of the stellar particle with ID 1984264::

  chemistryStats_getStarEnrichment chemistrystats.pkl --ID 1984264


Plot the distribution of the number of SNe that contributed to the chemical enrichment of stellar particles::

  chemistryStats_getStarsDistribution chemistrystats.pkl --xmin 1 --xmax 100
  
Plot the distribution of the number of SNe that contributed to the chemical enrichment of stellar particles excluding
SNe that contributed through the smoothed metallicity::

  
  chemistryStats_getStarsDistribution chemistrystats.pkl --xmin 1 --xmax 100 --NoSmooth

Plot the number of SNe a stellar particle has been enriched from, as a function of [Fe/H] vs [Mg/Fe]::

  chemistryStats_getStarsDistribution_FevsMgFe chemistrystats.pkl  snap/snapshot_0200.hdf5 --xmin -6 --xmax 0.5 --ymin -1 --ymax 1.5

The same but considering only progenitor stars more massive than 140 solar masses::

  chemistryStats_getStarsDistribution_FevsMgFe chemistrystats.pkl  snap/snapshot_0200.hdf5 --xmin -6 --xmax 0.5 --ymin -1 --ymax 1.5 --minMass 140

The same but considering only direct pollution and not pollution via smoothing::

  chemistryStats_getStarsDistribution_FevsMgFe chemistrystats.pkl  snap/snapshot_0200.hdf5 --xmin -6 --xmax 0.5 --ymin -1 --ymax 1.5 --NoSmooth







.. rubric:: Footnotes

.. [#f1] The mean mass is defined as ``0.5*(Mmax+Mmin)`` where
   ``Mmax`` is the mass of the star exploding just at the beginning of one timestep
   (the one with the shortest lifetime for this timestep) and
   ``Mmin`` is the mass of the star exploding just at the end of the timestep
   (the one with the largest lifetime for this timestep). For technical reason,
   we assume that all masses formed during the timestep have this mean mass.
   As the timesteps are very small, ``Mmin`` and ``Mmax`` are very similar.

