#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      gunits
#  brief:     
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################


kpc_in_cm = 3.0857e+21
year_in_s = 60 * 60 * 24 * 365  # 365.25
Msol_in_g = 1.989e33
G_in_cgs = 6.672e-8  # 6.67428e-8

UnitLength_in_cm = 1 * kpc_in_cm
UnitTime_in_s = 1e6 * year_in_s
UnitVelocity_in_cm_per_s = UnitLength_in_cm / UnitTime_in_s
G = 1.0

UnitMass_in_g = (UnitLength_in_cm) ** 3 / (UnitTime_in_s) ** 2 * G / G_in_cgs

print("UnitLength_in_cm         = %g" % (UnitLength_in_cm))
print("UnitLength_in_kpc        = %g" % (UnitLength_in_cm / kpc_in_cm))
print()
print("UnitTime_in_s            = %g" % (UnitTime_in_s))
print("UnitTime_in_yr           = %g" % (UnitTime_in_s / year_in_s))
print()
print("UnitMass_in_g            = %g" % (UnitMass_in_g))
print("UnitMass_in_Msol         = %g" % (UnitMass_in_g / Msol_in_g))
print()
print("UnitVelocity_in_cm_per_s = %g" % (UnitVelocity_in_cm_per_s))
print("UnitVelocity_in_km_per_s = %g" % (UnitVelocity_in_cm_per_s / 1e5))

print("G (code unit)            = %g" % (G_in_cgs / (UnitLength_in_cm) ** 3 * (UnitTime_in_s) ** 2 * UnitMass_in_g))
