#!/usr/bin/env python3

###########################################################################################
#  package:   Gtools
#  file:      gextract_sph.py
#  brief:     Extract and plot energy and mass values contained in the
#             output Gadget file called by default "energy.txt".
#  copyright: GPLv3
#             Copyright (C) 2019 EPFL (Ecole Polytechnique Federale de Lausanne)
#             LASTRO - Laboratory of Astrophysics of EPFL
#  author:    Yves Revaz <yves.revaz@epfl.ch>
#
# This file is part of Gtools.
###########################################################################################

"""
Extract and plot energy and mass values contained in the
output Gadget file called by default "energy.txt".
"""

import numpy as np
from pNbody import *
import string
import sys
import os
import copy as docopy

from pNbody import libgrid

from optparse import OptionParser
from Gtools import *
from Gtools import iofunc

import Ptools as pt


def parse_options():
    usage = "usage: %prog [options] file"
    parser = OptionParser(usage=usage)

    parser = pt.add_postscript_options(parser)
    parser = pt.add_limits_options(parser)
    parser = pt.add_log_options(parser)

    parser = pt.add_ftype_options(parser)
    parser = pt.add_reduc_options(parser)
    # parser = pt.add_center_options(parser)
    parser = pt.add_display_options(parser)
    # parser = pt.add_select_options(parser)
    parser = pt.add_cmd_options(parser)
    parser = pt.add_info_options(parser)

    parser.add_option("--Rmax",
                      action="store",
                      dest="Rmax",
                      type="float",
                      default=50.,
                      help="max radius of bins",
                      metavar=" FLOAT")

    parser.add_option("--nR",
                      action="store",
                      dest="nR",
                      type="int",
                      default=32,
                      help="number of bins in R",
                      metavar=" INT")

    parser.add_option("--eps",
                      action="store",
                      dest="eps",
                      type="float",
                      default=0.28,
                      help="smoothing length",
                      metavar=" FLOAT")

    parser.add_option("--nmin",
                      action="store",
                      dest="nmin",
                      type="float",
                      default=32,
                      help="min number of particles in a cell to accept value",
                      metavar=" INT")

    parser.add_option("--G",
                      action="store",
                      dest="G",
                      type="float",
                      default=1.,
                      help="Gravitational constant value",
                      metavar=" FLOAT")

    parser.add_option("--xmode",
                      action="store",
                      dest="xmode",
                      type="float",
                      default=2.,
                      help="mode for X-Toomre parameter",
                      metavar=" FLOAT")

    parser.add_option("--ErrTolTheta",
                      action="store",
                      dest="ErrTolTheta",
                      type="float",
                      default=0.5,
                      help="Error tolerance theta",
                      metavar=" FLOAT")

    parser.add_option("--AdaptativeSoftenning",
                      action="store_true",
                      dest="AdaptativeSoftenning",
                      default=False,
                      help="AdaptativeSoftenning")

    parser.add_option("--statfile",
                      action="store",
                      type="string",
                      dest="statfile",
                      default='stat.h5',
                      help="stat output file")

    parser.add_option("--components",
                      action="store",
                      dest="components",
                      type="string",
                      default="('gas','halo','disk','bulge','stars')",
                      help="list of components",
                      metavar=" TUPLE")

    parser.add_option("--x",
                      action="store",
                      type="string",
                      dest="x",
                      default='R',
                      help="x")

    parser.add_option("--y",
                      action="store",
                      type="string",
                      dest="y",
                      default='Den',
                      help="y")

    parser.add_option("--mode",
                      action="store",
                      type="string",
                      dest="mode",
                      default='all',
                      help="mode")

    (options, args) = parser.parse_args()

    if len(args) == 0:
        print("you must specify a filename")
        sys.exit(0)

    files = args

    return files, options


#######################################
# MakePlot
#######################################


def MakePlot(dirs, opt):
    # some inits
    palette = pt.GetPalette()
    colors = pt.SetColorsForFiles(files, palette)
    labels = []

    #######################################
    # deal with mode
    #######################################

    if opt.mode == 'all':
        opt.ComputePotential = True
        opt.ComputeDensity = True
        opt.ComputeDispertions = True
        opt.ComputeStability = True

    elif opt.mode == 'Den':
        opt.ComputePotential = False
        opt.ComputeDensity = True
        opt.ComputeDispertions = False
        opt.ComputeStability = False

    elif opt.mode == 'velocities':
        opt.ComputePotential = False
        opt.ComputeDensity = False
        opt.ComputeDispertions = True
        opt.ComputeStability = False

    elif opt.mode == 'stability':
        opt.ComputePotential = True
        opt.ComputeDensity = True
        opt.ComputeDispertions = True
        opt.ComputeStability = True

    #######################################
    # LOOP
    #######################################

    # read files
    for file in files:

        ######################################
        # open file and apply option
        ######################################
        nb = Nbody(file, ftype=opt.ftype)

        nb = pt.do_reduc_options(nb, opt)
        nb = pt.do_cmd_options(nb, opt)

        ###############
        # create total
        ###############

        nbt = docopy.deepcopy(nb)

        ######################################################
        # here, we should loop over component

        if opt.components != None:
            opt.components = eval(opt.components, dict(__builtins__=None))
            print(("components = ", opt.components))

        for component in opt.components:

            print("-----------------------------")
            print(("-- component = ", component))
            print("-----------------------------")

            nb = nbt.select(component)
            opt.statfile = "%s.h5" % (component)

            if nb.nbody > 0:

                ###############
                # other info
                ###############
                # nb = pt.do_center_options(nb,opt)
                nb = pt.do_info_options(nb, opt)
                nb = pt.do_display_options(nb, opt)

                ######################################
                # computes values
                ######################################

                # create cylindrical rt grid
                rc = 1.
                g = lambda r: np.log(r / rc + 1.)
                gm = lambda r: rc * (np.exp(r) - 1.)
                g = None;
                gm = None
                Gr = libgrid.Spherical_1d_Grid(rmin=0, rmax=opt.Rmax, nr=opt.nR, g=g, gm=gm)

                # build the tree
                if opt.ComputePotential:
                    print("ComputeTree")
                    nbt.getTree(force_computation=True, ErrTolTheta=opt.ErrTolTheta)
                    nb.getTree(force_computation=True, ErrTolTheta=opt.ErrTolTheta)

                # radius
                R = Gr.get_r()

                stats = {}

                stats['nR'] = opt.nR

                stats['Rmax'] = opt.Rmax
                stats['eps'] = opt.eps
                stats['ErrTolTheta'] = opt.ErrTolTheta
                stats['AdaptativeSoftenning'] = opt.AdaptativeSoftenning
                stats['xmode'] = opt.xmode
                stats['nmin'] = opt.nmin
                stats['G'] = opt.G

                stats['R'] = R

                ###################################
                # Surface density
                ###################################

                if opt.ComputeDensity:
                    print("ComputeDensity")

                    Den = Gr.get_DensityMap(nb)

                    stats['Den'] = Den

                  ###################################
                  # rotation curve and frequencies
                  ###################################
                  #
                  # if opt.ComputePotential:
                  # print("ComputePotential")
                  #
                  #   # radial acceleration (total)
                  #   Accx,Accy,Accz = Gr.get_AccelerationMap(nbt,eps=opt.eps,UseTree=True,AdaptativeSoftenning=opt.AdaptativeSoftenning)
                  #   Ar = np.sqrt(Accx**2+Accy**2)
                  #   Ar = sum(Ar,1)/opt.nt
                  #   dPhit = Ar
                  #   d2Phit = libgrid.get_First_Derivative(dPhit,R)
                  #
                  #
                  #   kappat = libdisk.Kappa(R,dPhit,d2Phit)
                  #   omegat = libdisk.Omega(R,dPhit)
                  #   vct	 = libdisk.Vcirc(R,dPhit)
                  #   #nut    = libdisk.Nu(z,Phit)
                  #
                  #
                  #
                  #   # radial acceleration (selection)
                  #   Accx,Accy,Accz = Gr.get_AccelerationMap(nb,eps=opt.eps,UseTree=True,AdaptativeSoftenning=opt.AdaptativeSoftenning)
                  #   Ar = np.sqrt(Accx**2+Accy**2)
                  #   Ar = sum(Ar,1)/opt.nt
                  #   dPhi = Ar
                  #   d2Phi = libgrid.get_First_Derivative(dPhi,R)
                  #
                  #
                  #   kappa = libdisk.Kappa(R,dPhi,d2Phi)
                  #   omega = libdisk.Omega(R,dPhi)
                  #   vc	= libdisk.Vcirc(R,dPhi)
                  #   #nu	 = libdisk.Nu(z,Phi)
                  #
                  #
                  #
                  #   #stats['Phi']   = Phi
                  #   stats['dPhi']  = dPhi
                  #   stats['d2Phi'] = d2Phi
                  #
                  #   stats['kappa']  = kappa
                  #   stats['omega']  = omega
                  #   #stats['nu']     = nu
                  #
                  #
                  #   #stats['Phit']   = Phit
                  #   stats['dPhit']  = dPhit
                  #   stats['d2Phit'] = d2Phit
                  #
                  #   stats['kappat']  = kappat
                  #   stats['omegat']  = omegat
                  #   #stats['nut']     = nut
                  #
                  #   stats['vct']     = vct
                  #   stats['vc']	  = vc
                  #

                if opt.ComputeDispertions:
                    print("ComputeDispersions")

                    ###################################
                    # number of points per cell
                    ###################################

                    nn = Gr.get_NumberMap(nb)

                    ###################################
                    # mean velocities
                    ###################################

                    # no, wrong
                    # v  = np.sqrt(nb.vx()**2+nb.vy()**2+nb.vz()**2)
                    # vm  = Gr.get_MeanValMap(nb, v )

                    ###################################
                    # velocity dispertions
                    ###################################

                    # sv = Gr.get_SigmaValMap(nb,v)
                    sv = 0 * nn

                    stats['nn'] = nn
                    # stats['vm']	  = vm
                    stats['sv'] = sv

                ###################################
                # save hdf
                ##################s#################

                pt.io.write_hdf5(opt.statfile, stats)

                ###################################
                # now, plot
                ###################################

                x = stats[opt.x]
                y = stats[opt.y]

                xmin, xmax, ymin, ymax = pt.SetLimits(opt.xmin, opt.xmax, opt.ymin, opt.ymax, x, y, opt.log)

                pt.plot(x, y)

    pt.SetAxis(xmin, xmax, ymin, ymax, log=opt.log)
    pt.show()


if __name__ == '__main__':
    files, opt = parse_options()
    pt.InitPlot(files, opt)
    # pt.figure(figsize=(8*2,6*2))
    # pt.figure(dpi=10)
    pt.pcolors
    # fig = pt.gcf()
    # fig.subplots_adjust(left=0.1)
    # fig.subplots_adjust(right=1)
    # fig.subplots_adjust(bottom=0.12)
    # fig.subplots_adjust(top=0.95)
    # fig.subplots_adjust(wspace=0.25)
    # fig.subplots_adjust(hspace=0.02)

    MakePlot(files, opt)
    pt.EndPlot(files, opt)
